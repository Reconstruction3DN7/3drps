/* 
 * File:   filterEngine.hpp
 * Author: clement
 *
 * Created on 27 février 2014, 13:53
 */


#ifndef FILTERENGINE_HPP
#define	FILTERENGINE_HPP

#include "RadiusFilter.hpp"

class FilterEngine {
public:
    FilterEngine();
    /**
    * run a filtering process according to method specified in arguments
    * @param method 1 for statistical, 2 for radius, 12 or 21 for combination
    * @return true if OK, false if error
    */
   bool runFilter(std::string pathToPly);
};




#endif	/* FILTERENGINE_HPP */

