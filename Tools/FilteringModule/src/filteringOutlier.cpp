#include <iostream>
#include <pcl/io/ply_io.h>
#include <pcl/point_types.h>
#include <pcl/filters/statistical_outlier_removal.h>
#include <pcl/filters/radius_outlier_removal.h>
#include <string>

using namespace std;

int main (int argc, char** argv) {
    
    if (argc < 7 ) {
        cerr << "Usage: filteringOutlier <point_cloud> <meanK> <StddevMulThresh> <MinNeighborsInRadius> <Radius> <method>" << endl; 
        cerr << "Please don't specify the .ply" << endl;
	cerr << "For method : 1 = statistical outlier removal, 2 = radius outlier removal" << endl;
	cerr << "             you can combine like 12 and 21 to process two filters" << endl; // TODO
	return -1;
    } 
    
    string filename = argv[1] ;
    string pathToPLYFile = filename + ".ply";
    int meanK = atoi(argv[2]);
    float sdmt = atof(argv[3]);
    int mnir = atoi(argv[4]);
    float radius = atof(argv[5]);
    int method = atoi(argv[6]);

    cout << "file   : " << pathToPLYFile << endl;
    cout << "meanK  : " << meanK << endl;
    cout << "sdmt   : " << sdmt << endl;
    cout << "mnir   : " << mnir << endl;
    cout << "radius : " << radius << endl;
 
    pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud (new pcl::PointCloud<pcl::PointXYZRGB>);
    pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud_filtered (new pcl::PointCloud<pcl::PointXYZRGB>);

    // Fill in the cloud data
    pcl::PLYReader reader;
    // Replace the path below with the path where you saved your file
    reader.read<pcl::PointXYZRGB> ( pathToPLYFile, *cloud );

    cout << "Cloud before filtering: " << endl;
    cout << *cloud << endl;
    cout << "test : " << (*cloud).at(0,100) << endl;

    // Create the filtering object for statistical outlier removal
    pcl::StatisticalOutlierRemoval<pcl::PointXYZRGB> sor;
    sor.setInputCloud (cloud);
    sor.setMeanK (meanK);
    sor.setStddevMulThresh (sdmt);

    // Create the filtering object for radius outlier removal
    pcl::RadiusOutlierRemoval<pcl::PointXYZRGB> ror;
    ror.setInputCloud (cloud);
    ror.setRadiusSearch(radius);
    ror.setMinNeighborsInRadius(mnir);

    // Process the filters
    switch(method) {

	case 1 : {   // stat only
	    cout << "processing Statistical Outlier Removal" << endl;
	    sor.filter (*cloud_filtered);
	    break;
	}

	case 2 : {    // radius only
	    cout << "processing Radius Outlier Removal" << endl;
	    ror.filter (*cloud_filtered);
	    break;
	}

	default : {  // error
	    cerr << "unvalid method" << endl;
	    return (-1);
	}
    }

    cout << "Cloud after filtering: " << endl;
    cout << *cloud_filtered << endl;
    
    


    // PLY files exporting
    pcl::PLYWriter writer;
    writer.write<pcl::PointXYZRGB> (filename + "inlier.ply", *cloud_filtered, false);

    sor.setNegative (true);
    sor.filter (*cloud_filtered);
    writer.write<pcl::PointXYZRGB> ( filename + "outlier.ply", *cloud_filtered, false);

    return (0);
}
