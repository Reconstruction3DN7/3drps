/* 
 * File:   FilterRPS.hpp
 * Author: clement
 *
 * Created on 27 février 2014, 13:14
 */

#ifndef FILTERRPS_HPP
#define	FILTERRPS_HPP

#include <string>
#include <iostream>
#include <pcl/io/ply_io.h>
#include <pcl/point_types.h>
#include <pcl/filters/statistical_outlier_removal.h>

class FilterRPS {
public:
    FilterRPS(std::string i_pathToPly);
    //FilterRPS(const FilterRPS& orig);
    virtual ~FilterRPS();
    
    /**
     * Apply the filter
     * @return 0 if filter is applied, -1 if error
     */
    virtual int mapplyFilter();
    /**
     * Estimate the parameters of the filter
     * @return 
     */
    virtual int estimateParameters();
private:
protected:
    std::string pathToPly;
    pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloudInitial ;
    pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloudFiltered;
};

#endif	/* FILTER_HPP */

