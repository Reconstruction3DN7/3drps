/* 
 * File:   RadiusFilter.cpp
 * Author: clement
 * 
 * Created on 27 février 2014, 13:17
 */

#include "RadiusFilter.hpp"

RadiusFilter::RadiusFilter(std::string i_pathToPly) : FilterRPS(i_pathToPly) {
    ror = new pcl::RadiusOutlierRemoval<pcl::PointXYZRGB>();
    ror->setInputCloud(cloudInitial);
}

/*RadiusFilter::RadiusFilter(const RadiusFilter& orig) {
}*/

RadiusFilter::~RadiusFilter() {
}

int RadiusFilter::mapplyFilter() {
    ror->setRadiusSearch(radius);
    ror->setMinNeighborsInRadius(minNeighborsInRad);
    
    ror->filter(*cloudFiltered);
    
    std::cout << cloudInitial->height << " " << cloudInitial->width << std::endl;
    std::cout << cloudFiltered->height << " " << cloudFiltered->width << std::endl;
    
    std::cout << cloudInitial->points[0] << std::endl;
    
    // PLY files exporting
    pcl::PLYWriter writer;
    writer.write<pcl::PointXYZRGB> (pathToPly + "inlier.ply", *cloudFiltered, false);
    
    return 0;
}

int RadiusFilter::estimateParameters() {
    radius = 0.5;
    minNeighborsInRad = 750;
    
    pcl::KdTreeFLANN<pcl::PointXYZRGB> kdtree;
    
    std::vector<int> pointIdxRadiusSearch;
    std::vector<float> pointRadiusSquaredDistance;
    pointIdxRadiusSearch.resize(100);
    pointRadiusSquaredDistance.resize(100);
    
    float searchRadius = 0.1;
    
    pcl::PointXYZRGB searchPoint;
    std::cout<<cloudInitial->points[1000].x<<std::endl;
    searchPoint.x = cloudInitial->points[1000].x;
    searchPoint.y = cloudInitial->points[1000].y;
    searchPoint.z = cloudInitial->points[1000].z;
    searchPoint.rgba = cloudInitial->points[1000].rgba;
    
    pointIdxRadiusSearch[0] = 1;
    std::cout << searchPoint << std::endl;
    std::cout << pointIdxRadiusSearch.at(0) << std::endl;
    //std::cout << "LOL" << std::endl;
    
    //std::cout << "neighbors of 1000 : " << kdtree.nearestKSearch(searchPoint, 100, pointIdxRadiusSearch, pointRadiusSquaredDistance) << std::endl;
    
    
    // Fill a vector with random indices
    // Fill a vector with number of neighbors in a radius for each indices
    
    
    return 0;
}

