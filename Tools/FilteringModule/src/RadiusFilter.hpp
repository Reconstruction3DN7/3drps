/* 
 * File:   RadiusFilter.hpp
 * Author: clement
 *
 * Created on 27 février 2014, 13:17
 */

#ifndef RADIUSFILTER_HPP
#define	RADIUSFILTER_HPP

#include "FilterRPS.hpp"
#include <pcl/filters/radius_outlier_removal.h>
#include <pcl/filters/statistical_outlier_removal.h>
#include <pcl/kdtree/kdtree_flann.h>
#include <vector>
#include <ctime>

class RadiusFilter : public FilterRPS {
public:
    RadiusFilter(std::string i_pathToPly);
    //RadiusFilter(const RadiusFilter& orig);
    virtual ~RadiusFilter();
    /**
     * Apply the filter
     * @return 0 if filter is applied, -1 if error
     */
    virtual int mapplyFilter();
    /**
     * Estimate the parameters of the filter
     * @return 
     */
    virtual int estimateParameters();
private:
    pcl::RadiusOutlierRemoval<pcl::PointXYZRGB>* ror;
    int minNeighborsInRad;
    float radius;

};

#endif	/* RADIUSFILTER_HPP */

