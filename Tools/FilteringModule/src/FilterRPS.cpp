/* 
 * File:   FilterRPS.cpp
 * Author: clement
 * 
 * Created on 27 février 2014, 13:14
 */

#include "FilterRPS.hpp"

FilterRPS::FilterRPS(std::string i_pathToPly) : cloudInitial(new pcl::PointCloud<pcl::PointXYZRGB>), 
                                                cloudFiltered(new pcl::PointCloud<pcl::PointXYZRGB>) {
    pathToPly = i_pathToPly;
    // Fill in the cloud data
    pcl::PLYReader reader;
    // Initialization of clouds
    // ...
    // Replace the path below with the path where you saved your file
    reader.read<pcl::PointXYZRGB> ( pathToPly + ".ply", *cloudInitial );
}

/*FilterRPS::FilterRPS(const Filter& orig) {
}*/

FilterRPS::~FilterRPS() {
}

int FilterRPS::mapplyFilter(){
}

int FilterRPS::estimateParameters() {
}


