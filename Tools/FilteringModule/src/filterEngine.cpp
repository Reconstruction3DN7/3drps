#include "filterEngine.hpp"

FilterEngine::FilterEngine() {
    
}

bool FilterEngine::runFilter(std::string pathToPly) {
    RadiusFilter rdFilter(pathToPly);
    
    if (rdFilter.estimateParameters() != 0) {
        std::cout << "Impossible to estimate parameters for radius fitler." << std::endl;
        return false;
    }
    
    if (rdFilter.mapplyFilter() != 0) {
        std::cout << "Impossible to apply radius filter." << std::endl;
        return false;
    }
    
    return true;
}
