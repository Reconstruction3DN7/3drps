set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${PROJECT_BINARY_DIR}/bin)
set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY ${PROJECT_BINARY_DIR}/lib)
set(CMAKE_LIBRARY_OUTPUT_DIRECTORY ${PROJECT_BINARY_DIR}/lib )

file(

        GLOB_RECURSE

        source_files_filter

        src/filterEngine.hpp
        src/FilterRPS.hpp
        src/RadiusFilter.hpp

        src/filterEngine.cpp
        src/FilterRPS.cpp
        src/RadiusFilter.cpp
#        src/testFilterEngine.cpp

)

ADD_LIBRARY(filteringModule ${source_files_filter})
TARGET_LINK_LIBRARIES(filteringModule ${PCL_LIBRARIES})

#ADD_EXECUTABLE( testFilterEngine ${source_files_filter})
#TARGET_LINK_LIBRARIES( testFilterEngine filteringModule ) 