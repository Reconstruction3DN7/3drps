/* 
 * File:   HSVColorimetricSpace.cpp
 * Author: athollon
 * 
 * Created on 24 février 2014, 15:53
 */

#include "colorimetricSpace/HSVColorimetricSpace.hpp"

using namespace std;
using namespace cv;

HSVColorimetricSpace::HSVColorimetricSpace(): ColorimetricSpace(3) 
{    
    m_allChannelsRange = new const float*[3];
    
    float* hChannelRange = new float[2];
    hChannelRange[0] = 0;
    hChannelRange[1] = 179;
    float* sChannelRange = new float[2];
    sChannelRange[0] = 0;
    sChannelRange[1] = 255;
    float* vChannelRange = new float[2];
    vChannelRange[0] = 0;
    vChannelRange[1] = 255;
   
    m_allChannelsRange[0] = hChannelRange;
    m_allChannelsRange[1] = sChannelRange;
    m_allChannelsRange[2] = vChannelRange;
    
    vector<int> binList;
    binList.push_back(30);
    binList.push_back(32);
    binList.push_back(50);
    
    m_binList = binList;
    
    vector<string> channelsName;
    channelsName.push_back("hue");
    channelsName.push_back("saturation");
    channelsName.push_back("value");
    m_channelsName = channelsName;
    cout << m_nbOfChannels << "\n";
}

HSVColorimetricSpace::HSVColorimetricSpace(const HSVColorimetricSpace& orig) {
}

HSVColorimetricSpace::~HSVColorimetricSpace() {
}

Mat HSVColorimetricSpace::convertIntoCurrentColorimetricSpace(cv::Mat initialImg) {
    Mat hsvImg;
    cvtColor(initialImg, hsvImg, CV_RGB2HSV);
    return hsvImg;
}

