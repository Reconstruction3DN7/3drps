/* 
 * File:   UserAssistMaskCreation.cpp
 * Author: athollon
 * 
 * Created on 1 mars 2014, 10:51
 */

#include "UserAssistMaskCreation.hpp"
using namespace cv;
using namespace std;

Mat wshed, img;
vector<Vec3b> lastColors;



UserAssistMaskCreation::UserAssistMaskCreation() {
}

UserAssistMaskCreation::UserAssistMaskCreation(const UserAssistMaskCreation& orig) {
}

UserAssistMaskCreation::~UserAssistMaskCreation() {
}

void UserAssistMaskCreation::help() {
    cout << "\nThis program demonstrates the famous watershed segmentation algorithm in OpenCV: watershed()\n"
            "Usage:\n"
            "./watershed [image_name -- default is fruits.jpg]\n" << endl;


    cout << "Hot keys: \n"
        "\tESC - quit the program\n"
        "\tr - restore the original image\n"
        "\tw or SPACE - run watershed segmentation algorithm\n"
        "\tdouble click on the area after you compute the watershed segmetation\n"
        "\ts - save the selected area and generate the mask\n"
        "\t\t(before running it, *roughly* mark the areas to segment on the image)\n"
        "\t  (before that, roughly outline several markers on the image)\n";
}

/**
 *  Return true if color is in the lastColors list
 * @param lastColors: list of the color which were selected by the user
 * @param color : current Color
 * @return 
 */
bool UserAssistMaskCreation::isColorSelected(std::vector<cv::Vec3b> lastColors, cv::Vec3b color) {
    for (int i = 0; i < lastColors.size(); i++) {
        if (color == lastColors[i]) {
            return true;

        }
    }
    return false;
}

/**
   * Check if pixel around the pixel(i,j) is selected, ie is white
   * @param whsed: the watershed Matrix where the check is done
   * @param i 
   * @param j
   * @return 
   */
bool UserAssistMaskCreation::isNeightborSelected(cv::Mat wshed, int i, int j) {
    Vec3b downNeightbor;
    Vec3b topNeightbor; 
    Vec3b rightNeightbor;
    Vec3b leftNeightbor;
    if ( i == 0 ) {
        topNeightbor = Vec3b(0,0,0);
    } else {
        topNeightbor = wshed.at<Vec3b>(i-1,j);
    }
    
    if ( j == 0 ) {
        leftNeightbor = Vec3b(0,0,0);
    } else {
        leftNeightbor = wshed.at<Vec3b>(i,j-1);
    }
    
    if ( j > wshed.cols ) {
        rightNeightbor = Vec3b(0,0,0);
    } else {
        rightNeightbor = wshed.at<Vec3b>(i,j+1);
    }
    
    if ( i > wshed.rows ) {
        downNeightbor = Vec3b(0,0,0);
    } else {
        downNeightbor = wshed.at<Vec3b>(i+1,j-1);
    }
    
    if (isColorSelected(m_lastColors, downNeightbor) || 
        isColorSelected(m_lastColors, topNeightbor) ||
        isColorSelected(m_lastColors, rightNeightbor) ||
        isColorSelected(m_lastColors, topNeightbor)) {
        return true;
    } else {
        return false;
    }
}

/**
 *  Event listener based on mouse event on the image. Used to create marker
 * @param event : event on the figure on which we create markers
 * @param x : the position of the mouse .x
 * @param y : the position of the mouse .y
 * @param flags : flags raised during the event
 * @param 
 */
void UserAssistMaskCreation::onMouseUAMC(int event, int x, int y, int flags) {
    if( x < 0 || x >= m_img.cols || y < 0 || y >= m_img.rows )
        return;
    if( event == CV_EVENT_LBUTTONUP || !(flags & CV_EVENT_FLAG_LBUTTON) )
        m_prevPt = Point(-1,-1);
    else if( event == CV_EVENT_LBUTTONDOWN )
        m_prevPt = Point(x,y);
    else if( event == CV_EVENT_MOUSEMOVE && (flags & CV_EVENT_FLAG_LBUTTON) ) {
        Point pt(x, y);
        if( m_prevPt.x < 0 )
            m_prevPt = pt;
        line( m_markerMask, m_prevPt, pt, Scalar::all(255), 5, 8, 0 );
        line( m_img, m_prevPt, pt, Scalar::all(255), 5, 8, 0 );
        m_prevPt = pt;
        imshow("image", m_img);
    }
}

void UserAssistMaskCreation::selectAreaGUI(vector<Vec3b> lastColors, Mat initialImg, Mat modifiedImg) {
    for (int i = 0; i < initialImg.rows; i++) {
        for (int j = 0; j < initialImg.cols; j++) {
            Vec3b currentColor = initialImg.at<Vec3b>(i,j);
            if (isColorSelected(lastColors, currentColor)) {
                modifiedImg.at<Vec3b>(i,j) = Vec3b(255, 255, 255);
            } else {
                modifiedImg.at<Vec3b>(i,j) = initialImg.at<Vec3b>(i,j);
            }
        }
    }
}

/**
 *  Event listener based on mouse event on the wshed image. Used to select an Area
 * @param img: image on which the event listener is used
 * @param event : event on the figure on which we create markers
 * @param x : the position of the mouse .x
 * @param y : the position of the mouse .y
 * @param flags : flags raised during the event
 */
void UserAssistMaskCreation::onMouseSelectAreaUAMC(int event, int x, int y, int flags) {
    Point selectedPt(-1,-1);
    if( x < 0 || x >= m_img.cols || y < 0 || y >= m_img.rows )
        return;
    else if ( event == CV_EVENT_LBUTTONDOWN && (flags & CV_EVENT_FLAG_CTRLKEY)) {
        selectedPt = Point(x,y);
        Mat wshedCopy = Mat::zeros(m_wshed.size(), CV_8UC3);
        Vec3b currentColor = m_wshed.at<Vec3b>(y,x);
        m_lastColors.push_back(currentColor);
        selectAreaGUI(m_lastColors, m_wshed, wshedCopy);
        imshow("watershed transform", wshedCopy);
    } else if ( event == CV_EVENT_LBUTTONDBLCLK ) {
        m_lastColors.clear();
        selectedPt = Point(x,y);
        Mat wshedCopy = Mat::zeros(m_wshed.size(), CV_8UC3);
        Vec3b currentColor = m_wshed.at<Vec3b>(y,x);
        m_lastColors.push_back(currentColor);
        selectAreaGUI(m_lastColors, m_wshed, wshedCopy);
        imshow("watershed transform", wshedCopy);
    }
}

/**
 * Generate a color for each area
 * @param numberOfArea
 * @return 
 */
std::vector<cv::Vec3b> UserAssistMaskCreation::generateColorForWatershed(int numberOfArea) {
    vector<Vec3b> colorTab;
    for(int i = 0; i < numberOfArea; i++ ) {
        int b = theRNG().uniform(0, 255);
        int g = theRNG().uniform(0, 255);
        int r = theRNG().uniform(0, 255);
        colorTab.push_back(Vec3b((uchar)b, (uchar)g, (uchar)r));
    }
    return colorTab;
}

/**
  * Run watershed based on marker used by the user
  * @param img0 : the initial image
  * @param markers : the markers defined by the user
  * @param numberOfArea : the number of Area found by find contours
  * @param colorTab : the color for each area
  * @return 
  */
 cv::Mat UserAssistMaskCreation::runAndShowWatershed(cv::Mat img0, cv::Mat markers, int numberOfArea, 
                     std::vector<cv::Vec3b> colorTab) {
    Mat watershedImg;
    double t = (double)getTickCount();
    watershed( img0, markers );
    t = (double)getTickCount() - t;
    printf( "execution time = %gms\n", t*1000./getTickFrequency() );

    watershedImg = Mat::zeros(markers.size(), CV_8UC3);
    watershedImg = Scalar::all(0);
    // paint the watershed image
    for( int i = 0; i < markers.rows; i++ ) {
        for( int j = 0; j < markers.cols; j++ ) {
            int index = markers.at<int>(i,j);
            if( index == -1 )
                watershedImg.at<Vec3b>(i,j) = Vec3b(255,255,255);
            else if( index <= 0 || index > numberOfArea )
                watershedImg.at<Vec3b>(i,j) = Vec3b(255,255,255);
            else
                watershedImg.at<Vec3b>(i,j) = colorTab[index - 1];
        }
    }
    return watershedImg;
 }
 
 void UserAssistMaskCreation::onMouse(int event, int x, int y, int flags, void* param) {
     static_cast<UserAssistMaskCreation*>(param)->onMouseUAMC(event, x, y, flags);
 }
 
 void UserAssistMaskCreation::onMouseSelectArea(int event, int x, int y, int flags, void* param) {
     static_cast<UserAssistMaskCreation*>(param)->onMouseSelectAreaUAMC(event, x, y, flags);
 }
 
void UserAssistMaskCreation::createMaskFromWatershed(Mat wshed, string maskFilePath, string maskForClassifierFilePath, int nbMaskCreated) {
    string maskExtension = generateMaskExtension(nbMaskCreated);
    string maskForClassifierFileName = maskForClassifierFilePath + maskExtension;
    string maskFileName = maskFilePath + ".mask";
    ofstream outputStream(maskFileName.c_str());
    
    if (!outputStream) {
        cout << "issue while creating the output file";
    }
    
    outputStream << wshed.rows << "\n " << wshed.cols << "\n" ;
    Mat mask = Mat::zeros(wshed.rows, wshed.cols, CV_8UC1);
    for (int i = 0; i < wshed.rows; i++) {
        for (int j = 0 ; j < wshed.cols; j++) {
            Vec3b currentColor = wshed.at<Vec3b>(i,j);
            if (currentColor == Vec3b(255,255,255)) {
                if (isNeightborSelected(wshed, i,j)) {
                    mask.at<uchar>(i,j) = 255;
                    outputStream << 1;
                } else {
                     mask.at<uchar>(i,j) = 0;
                     outputStream << 0;
                }
            } else {
                if (isColorSelected(m_lastColors, currentColor)) {
                    mask.at<uchar>(i,j) = 255;
                    outputStream << 1;
                } else {
                    mask.at<uchar>(i,j) = 0;
                    outputStream << 0;
                }
            }
         }
         outputStream << "\n";
    }
    
    // We save the foreground/background and support mask on the folder from which
    // classifier need data.
    imwrite(maskForClassifierFileName + ".jpg", mask);
    
    // If the nbMaskCreated ==0 then we are actually defining the foreground mask
    //which could be used by pmvs. We generate it in the mask Folder only on this
    //condition.
    if (nbMaskCreated == 0) {
        imwrite(maskFilePath + ".pgm", mask); 
    }
    namedWindow("mask" + maskExtension);
    imshow("mask" + maskExtension, mask);
}

/**
* 
* @param imageName : name of the image on which we need a watershed ex: temple00001
* @param extension : extension of the image ex: .jpg, .png
* @param imagePath : path where we could find the image
* @param maskOutputPath : path where the mask should be saved
* @param maskForClassifierOutputPath : path where the masks used for classifier should be written
*/
void UserAssistMaskCreation::processWatershedUserAssistMaskCreation(string imageName, string extension,
                                string imagePath, string maskOutputPath, 
                                string maskForClassifierOutputPath ){

     
    string filename = imagePath + imageName + extension ; // the <total> path of the picture read
    string outputMaskFileName = maskOutputPath + imageName; // the path of the output .mask and .pgm
    string maskForClassifierFileName = maskForClassifierOutputPath + imageName; //the path of the output folder for classifier
    
    Mat img0 = imread(filename, 1), imgGray;
    if( img0.empty() ) {
        cerr << "Couldn't open image " << filename 
             << "\n";
    }
    
    help();
    namedWindow( "image", 1 );
    
    img0.copyTo(m_img);    
    cvtColor(m_img, m_markerMask, CV_BGR2GRAY);
    cvtColor(m_markerMask, imgGray, CV_GRAY2BGR);
    
    m_prevPt = Point(-1,-1);
    
    m_markerMask = Scalar::all(0);
    imshow( "image", m_img );
    setMouseCallback( "image", UserAssistMaskCreation::onMouse, this);
    
    for(;;) {
        int c = waitKey(0);

        if( (char)c == 27 )
            break;

        if( (char)c == 'r' ) {
            m_markerMask = Scalar::all(0);
            img0.copyTo(m_img);
            imshow( "image", m_img );
        }

        if( (char)c == 'w' || (char)c == ' ' ) {
            int i, j, compCount = 0;
            vector<vector<Point> > contours;
            vector<Vec4i> hierarchy;

            findContours(m_markerMask, contours, hierarchy, CV_RETR_CCOMP, CV_CHAIN_APPROX_SIMPLE);

            if( contours.empty() )
                continue;
            Mat markers(m_markerMask.size(), CV_32S);
            markers = Scalar::all(0);
            int idx = 0;
            for( ; idx >= 0; idx = hierarchy[idx][0], compCount++ )
                drawContours(markers, contours, idx, Scalar::all(compCount+1), -1, 8, hierarchy, INT_MAX);

            if( compCount == 0 )
                continue;

            vector<Vec3b> colorTab = generateColorForWatershed(compCount);
            m_wshed = runAndShowWatershed(img0, markers, compCount, colorTab);
            imshow( "watershed transform", m_wshed );
            setMouseCallback("watershed transform", onMouseSelectArea, this);

            int nbMaskCreated =0;
            for (;;) {
                int k = waitKey(0);
                if( (char)k == 's') {
                    createMaskFromWatershed(m_wshed, outputMaskFileName, maskForClassifierFileName, nbMaskCreated);
                    nbMaskCreated++;
                }
                if( (char)k == 27 )
                    break;
            }
            break;
        }
    }

}

/**
 * 
 * @param imagesName
 * @param extension
 * @param imagePath
 * @param maskOutputPath
 * @param maskForClassifierOutputPath
 */
    
void UserAssistMaskCreation::processWatershedUserAssistMaskCreation(vector<string> imagesName, string extension,
                                           string imagePath, string maskOutputPath,
                                           string maskForClassifierOutputPath ) {

    try {
        if (imagesName.size() == 0) {
            throw string("image name not specified");
        } 
        
        for (int i = 0; i < imagesName.size(); i++) {
            processWatershedUserAssistMaskCreation(imagesName[i], extension, imagePath,
                                      maskOutputPath, maskForClassifierOutputPath);
        }
        cv::destroyAllWindows();

    } catch (const string& exception) {
        cerr << exception;
    }

}

std::string UserAssistMaskCreation::generateMaskExtension(int nbMaskCreated) {
    string maskExtension; 
    switch(nbMaskCreated) {
        case 0: 
            maskExtension = ".foreground";
            break;
        case 1:
            return ".background";
             break;
        case 2:
            return ".support";
            break;
    }
    return maskExtension;
}