/* 
 * File:   AutomaticMaskCreation.hpp
 * Author: athollon
 *
 * Created on 3 mars 2014, 22:05
 */

#ifndef AUTOMATICMASKCREATION_HPP
#define	AUTOMATICMASKCREATION_HPP
#include <string>
#include <vector>
#include "opencv2/imgproc/imgproc.hpp"
#include <iostream>
#include <fstream>

class AutomaticMaskCreation {
public:
    AutomaticMaskCreation();
    AutomaticMaskCreation(const AutomaticMaskCreation& orig);
    virtual ~AutomaticMaskCreation();
    
    /**
     * 
     * @param imagesName the root name of each image which need to be classify
     *      example : 0000001
     * @param imagesForClassifier : the root name of each image used previously with
     *         the userAssistMaskCreation
     * @param extension : the extension of the files
     * @param imagePath : the path of the images /input/
     * @param maskOutputPath : the output for the mask
     * @param maskForClassifierPath :  the path of the mask used by the classifier
     */
    void processAutomaticMaskCreation(std::vector<std::string> imagesName, std::vector<std::string> imagesForClassifier,
                                    std::string extension, std::string imagePath, std::string maskOutputPath,
                                    std::string maskForClassifierPath);
private:
    

};

#endif	/* AUTOMATICMASKCREATION_HPP */

