/* 
 * File:   HistClassifier.cpp
 * Author: athollon
 * 
 * Created on 23 février 2014, 13:03
 */

#include <classifier/HistClassifier.hpp>

using namespace std;
using namespace cv;

HistClassifier::HistClassifier() {
}

HistClassifier::HistClassifier(std::vector<std::string> imageNames, std::string imgPath, std::string masksPath, std::string extension) {
    m_datasetSize = 0;
    initializeDatasetData(imageNames, imgPath, masksPath, extension);
}

HistClassifier::HistClassifier(const HistClassifier& orig) {
}

HistClassifier::~HistClassifier() {
}

void HistClassifier::showChannelsOfImgInColorimetricSpace(int imageNumber) {
    try {
        if ( imageNumber >= 0 && imageNumber < m_datasetSize ) {
            Mat img = m_datasetImg[imageNumber];
            m_colorimetricSpace->showImgInColorimetricSpace(img);
        } else {
            throw("the image specified cannot be shown. Wrong image Number \n");
        }
    } catch (string const& exception) {
        cerr << exception;
    }

}

MatND HistClassifier::getHistogramOfSpecifiedPicture(int imageNumber, vector<int> selectedChannelsId, int maskType, ColorimetricSpace* colorimetricSpace) {
 try {
        
        //Checking validity of the id of the selected channel
        int nbOfChannelsSelected = selectedChannelsId.size();
        if ( nbOfChannelsSelected == 0 ) {
            throw string ("There is no channel selected for Histogram Creation");
        }
        
        for (int i = 0; i < nbOfChannelsSelected ; i++) {
            if (selectedChannelsId[i] > colorimetricSpace->getNbOfChannels()) {
                throw string ("Channel are not well defined for Histogram Creation");
            }
        }
        
        //Checking validity of the image specified
        if (!(imageNumber >= 0 && imageNumber < m_datasetSize)) {
            throw string ("the image specified cannot be shown. Wrong image Number \n");
        } 
        
        Mat convertedImage = colorimetricSpace->convertIntoCurrentColorimetricSpace(m_datasetImg[imageNumber]);
        
        vector<int> binList = colorimetricSpace->getBinsValue();
        const float** rangeList = colorimetricSpace->getAllChannelsRange();
       
        int binForHist[nbOfChannelsSelected];
        int channels[nbOfChannelsSelected];
        int currentChannel;
        const float* ranges[nbOfChannelsSelected];
      
        for (int i = 0; i < nbOfChannelsSelected; i++) {
            currentChannel = selectedChannelsId[i];
            ranges[i] = rangeList[currentChannel];
            binForHist[i] = binList[currentChannel];
            channels[i] = currentChannel;
        }
        
        Mat mask;
        if (maskType == 0) {
            mask = m_foregroundMaskforDatasetImg[imageNumber];
        }
        if (maskType == 1) {
            mask = m_backgroundMaskForDatasetImg[imageNumber];
        }
        if (maskType == 2) {
            mask = m_supportMaskForDatasetImg[imageNumber];
        }
       
        MatND histForPicture;
        calcHist( &convertedImage, 1, channels, mask,
                histForPicture, 2, binForHist, ranges,
                true,
                false );        
        
      //  getBackProjection(histForPicture, m_datasetImg[imageNumber], selectedChannelsId, colorimetricSpace);
        return histForPicture;
        
    } catch (string const& exception) {
        cerr << exception;
    }
};

MatND HistClassifier::getHistogramOfSpecifiedPicture(int imageNumber, vector<int> selectedChannelsId, int maskType) {
    return getHistogramOfSpecifiedPicture(imageNumber, selectedChannelsId, maskType, m_colorimetricSpace);
}

void HistClassifier::initClassifierHist(vector<int> selectedChannelsId) {
    for (int j= 0 ; j < 3; j++ ) {
        cout << "j : " << j << "\n";
        initClassifierHist(selectedChannelsId, j);
    }
}

void HistClassifier::getImageMask(string currentImagePath, vector<int> selectedChannelsId, std::string maskOutputPath, std::string imageName) {
    MatND imageBackProjectionForeground;
    MatND imageBackProjectionBackground;
    MatND imageBackProjectionSupport;

    Mat imageToProcess = imread(currentImagePath, 1);
    
    imageBackProjectionForeground = getBackProjection(m_classifierHists[0], imageToProcess, selectedChannelsId, m_colorimetricSpace);
    
    imageBackProjectionBackground = getBackProjection(m_classifierHists[1], imageToProcess, selectedChannelsId, m_colorimetricSpace);
    
    imageBackProjectionSupport = getBackProjection(m_classifierHists[2], imageToProcess, selectedChannelsId, m_colorimetricSpace);
    
    Mat binBackProjImgForeground, binBackProjImgBackground, binBackProjImgSupport;
    threshold(imageBackProjectionForeground, binBackProjImgForeground, 0.2*255, 255, THRESH_BINARY );
    threshold(imageBackProjectionBackground, binBackProjImgBackground, 0.5*255, 255, THRESH_BINARY );
    threshold(imageBackProjectionSupport, binBackProjImgSupport, 0.5*255, 255, THRESH_BINARY );
    
    findMarkerAndComputeWatershed(binBackProjImgForeground, binBackProjImgBackground, binBackProjImgSupport, imageToProcess, maskOutputPath, imageName);
    
}


void HistClassifier::initClassifierHist(vector<int> selectedChannelsId, int maskType) {    
    try {
        
        if (m_datasetSize == 0) {
            throw string("dataset size = 0 \n");
        }
        
        //Initialisation of the foreground Hist

        MatND hist;
        cout << "size data set " << m_datasetSize ;
        
        for (int i = 0; i < m_datasetSize; i++) {
            cout << "data size : " << m_datasetSize << "\n";
            MatND currentHist = getHistogramOfSpecifiedPicture(i, selectedChannelsId, maskType);
            cerr << "done get histo";
            cout << i << "\n";
            if (i == 0) {
               hist = currentHist;
            } else {
               hist = hist + currentHist;
           }
        }
       
        hist = hist / m_datasetSize;
        m_classifierHists.push_back(hist);
    
        
        
    } catch (const string& exception) {
        cerr << exception;
    }
    
}



void HistClassifier::showImgAndMask(int imageNumber) {
    try {
        if ( imageNumber >= 0 && imageNumber < m_datasetSize ) {
            namedWindow("picture",1);
            namedWindow("foreground mask", 1);
            namedWindow("support mask", 1);
            namedWindow("background mask", 1);

            imshow("picture", m_datasetImg[imageNumber]);
            imshow("foreground mask", m_foregroundMaskforDatasetImg[imageNumber]);
            imshow("support mask", m_supportMaskForDatasetImg[imageNumber]);
            imshow("background mask", m_backgroundMaskForDatasetImg[imageNumber]);
            waitKey();
        } else {
            throw string ("the image specified cannot be shown. Wrong image Number \n");
        }
    } catch (string const& exception) {
        cerr << exception;
    }
}

void HistClassifier::setColorimetricSpace(ColorimetricSpace* colorimetricSpace) {
    m_colorimetricSpace = colorimetricSpace;
}

void HistClassifier::showHistogram(cv::MatND histogram, std::vector<int> selectedChannelsId, ColorimetricSpace* colorimetricSpace){
    int nbOfChannelsSelected = selectedChannelsId.size();
    
    try {
        if (nbOfChannelsSelected == 3) {
            throw string("3 channels selected, the 3D hist can't be printed");
        }

        // Getting the write parameters for the histogram representation
        vector<int> allBinList = colorimetricSpace->getBinsValue();
        vector<string> allChannelsName = colorimetricSpace->getChannelsName();
        string selectedChannelsName = "";
        int binForHist[nbOfChannelsSelected];
        int channels[nbOfChannelsSelected];
        int currentChannel;

        for (int i = 0; i < nbOfChannelsSelected; i++) {
            currentChannel = selectedChannelsId[i];
            binForHist[i] = allBinList[currentChannel];
            channels[i] = currentChannel;
            selectedChannelsName = selectedChannelsName + allChannelsName[currentChannel] + " ";
        }
        
        string histName = selectedChannelsName + " Histogram";
        cout << histName;
        
        if (nbOfChannelsSelected == 2) {
            double histMaxVal = 0;
            minMaxLoc(histogram, 0, &histMaxVal, 0, 0);
            int scale = 10;

            Mat hsHistImg = Mat::zeros(binForHist[0]*scale, binForHist[1]*scale, CV_8UC3);

            //creation d'une representation graphique            
            
            for( int l = 0; l < binForHist[0]; l++ ) {
                    cout << "bin for hist " << binForHist[l] << "\n";
                for( int k = 0; k < binForHist[1]; k++ )
                {
                    cout << "bin for histk             " << binForHist[k] << "\n";
                    float binVal = histogram.at<float>(l, k);
                    int intensity = cvRound(binVal*255/histMaxVal);
                    rectangle( hsHistImg, Point(l*scale, k*scale),
                                Point( (l+1)*scale - 1, (k+1)*scale - 1),
                                Scalar::all(intensity),
                                CV_FILLED );
                }
            }
            namedWindow( histName, 1 );
            imshow( histName, hsHistImg );
            waitKey();
        }
    } catch (string const& exception) {
        cerr << exception;
    }

};


void HistClassifier::showHistogram(cv::MatND histogram, std::vector<int> selectedChannelsId) {
    showHistogram(histogram, selectedChannelsId, m_colorimetricSpace);
}


///////////////////////////PRIVATE FUNCTIONS/////////////////////

vector<string> HistClassifier::generateImgAndMaskName(string imgNameRoot, string imgPath, string maskPath, string extension) {
    string foregroundMaskName = maskPath + imgNameRoot + ".foreground.jpg";
    string backgroundMaskName = maskPath + imgNameRoot + ".background.jpg";
    string supportMaskName = maskPath + imgNameRoot + ".support.jpg";
    string imgName = imgPath + imgNameRoot + extension;
    
    vector<string> imgAndMasksName(0);
    
    imgAndMasksName.push_back(imgName);
    imgAndMasksName.push_back(foregroundMaskName);
    imgAndMasksName.push_back(backgroundMaskName);
    imgAndMasksName.push_back(supportMaskName);
    return imgAndMasksName;
      
}

int HistClassifier::initializeDatasetData(std::vector<std::string> imgNames, string imgPath, string maskPath, string extension) {
    
    int datasetSize = imgNames.size();
    if ( datasetSize <= 0 ) {
        return 1;
    }
    
    string currentImgName;
    vector<string> currentImgAndMasksName(0);
    Mat currentImg;
    Mat foregroundMask;
    Mat backgroundMask;
    Mat supportMask;
   
    for ( int i = 0; i < datasetSize; i++ ) {
        currentImgName = imgNames[i];
        currentImgAndMasksName = generateImgAndMaskName(currentImgName, imgPath, maskPath, extension);
        currentImg = imread(currentImgAndMasksName[0]);
        foregroundMask = imread(currentImgAndMasksName[1],0);
        backgroundMask = imread(currentImgAndMasksName[2],0);
        supportMask = imread(currentImgAndMasksName[3],0);
        
        try {
            if (foregroundMask.empty() || backgroundMask.empty() || supportMask.empty()) {
                throw i;
            } else {
                m_foregroundMaskforDatasetImg.push_back(foregroundMask);
                m_backgroundMaskForDatasetImg.push_back(backgroundMask);
                m_supportMaskForDatasetImg.push_back(supportMask);
                m_datasetImg.push_back(currentImg);
                m_datasetSize++;
            }
        } catch (int e) {
            cerr << "issue while acquisition of image number "  << e << " \n";
        }
    }
}

cv::MatND HistClassifier::getBackProjection(cv::MatND hist, Mat initialImage, vector<int> selectedChannels, ColorimetricSpace* colorimetricSpace) {
    int nbOfChannelsSelected = selectedChannels.size();
    Mat imgInColorSpace;
    MatND backProj;
    imgInColorSpace = colorimetricSpace->convertIntoCurrentColorimetricSpace(initialImage);
    normalize( hist, hist, 0, 255, NORM_MINMAX, -1, Mat());
    
    const float** rangeList = colorimetricSpace->getAllChannelsRange();
    vector<string> allChannelsName = colorimetricSpace->getChannelsName();
    int channels[nbOfChannelsSelected];
    int currentChannel;
    const float* ranges[nbOfChannelsSelected];
    string selectedChannelsName = "";

    for (int i = 0; i < nbOfChannelsSelected; i++) {
        currentChannel = selectedChannels[i];
        ranges[i] = rangeList[currentChannel];
        channels[i] = currentChannel;
        selectedChannelsName = selectedChannelsName + allChannelsName[currentChannel] + " ";

    }
   
    string resultName = selectedChannelsName + " Back Projection";
            
    calcBackProject( &imgInColorSpace, 1, channels, hist, backProj, ranges, 1, true );

//    namedWindow(resultName, 1);
//    imshow(resultName, backProj);
    
//    waitKey();
    
    return backProj;
}

void HistClassifier::findMarkerAndComputeWatershed(cv::Mat binForegroundBP,
                                        cv::Mat binBackgroundBP, cv::Mat binSupportBP, cv::Mat initialImage,
                                        std::string outputMaskPath, std::string imageName) {
    
    Mat foreground;
    foreground = binForegroundBP;
     
    int erosion_size = 1;
    Mat element = cv::getStructuringElement(cv::MORPH_RECT,
                      cv::Size(2 * erosion_size + 1, 2 * erosion_size + 1), 
                      cv::Point(erosion_size, erosion_size) );
   
    
    Mat background;
    erode(binBackgroundBP, background, element);
    threshold(background, background, 0.7*255, 50, THRESH_BINARY);
    
    Mat support;
    erode(binSupportBP, support, element);
    threshold(support, support, 0.5*255, 100, THRESH_BINARY);
    //namedWindow("background Threshold");
    //imshow("background Threshold", support);
    //waitKey();
    
    Mat marker;
    add(support, background, background);
    add(background, foreground, marker);
    namedWindow("Marker");
    imshow("Marker", marker);
    waitKey();
    
    Mat marker32 ;
    marker32 = marker.clone();
    marker32.convertTo(marker32, CV_32S);
    
    watershed(initialImage,marker32);
    convertScaleAbs(marker32, marker32);
   // namedWindow("Show marker32");
   // imshow("Show marker32",marker32);
   // waitKey();
    threshold(marker32, marker32, 250, 255, THRESH_BINARY);
    
   // imshow("Show marker32", marker32);
   //  waitKey();
    
    string maskFileName = outputMaskPath + imageName + ".mask";
    ofstream outputStream(maskFileName.c_str());
    
    if (!outputStream) {
        cout << "issue while creating the output file";
    }
    outputStream << marker32.rows << "\n" << marker32.cols << "\n" ;
    Mat mask = Mat::zeros(marker32.rows, marker32.cols, CV_8UC1);
    for (int i = 0; i < marker32.rows; i++) {
        for (int j = 0 ; j < marker32.cols; j++) {
            uchar currentColor = marker32.at<uchar>(i,j);
            if (currentColor == 255) {
                mask.at<uchar>(i,j) = 255;
                outputStream << 1;
            } else {
                mask.at<uchar>(i,j) = 0;
                outputStream << 0;
            }
         }
         outputStream << "\n";
    }
    
    // We save the foreground/background and support mask on the folder from which
    // classifier need data.
    imwrite(imageName + "mask.jpg", mask);
    
    //namedWindow(imageName + "mask");
    //imshow(imageName + "mask", mask);
    //waitKey();
}