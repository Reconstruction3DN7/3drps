#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/highgui/highgui.hpp"
#include <fstream>
#include <cstdio>
#include <iostream>
#include <stdlib.h>

using namespace cv;
using namespace std;

static void help() {
    cout << "\nThis program demonstrates the famous watershed segmentation algorithm in OpenCV: watershed()\n"
            "Usage:\n"
            "./watershed [image_name -- default is fruits.jpg]\n" << endl;


    cout << "Hot keys: \n"
        "\tESC - quit the program\n"
        "\tr - restore the original image\n"
        "\tw or SPACE - run watershed segmentation algorithm\n"
        "\tdouble click on the area after you compute the watershed segmetation\n"
        "\ts - save the selected area and generate the mask\n"
        "\t\t(before running it, *roughly* mark the areas to segment on the image)\n"
        "\t  (before that, roughly outline several markers on the image)\n";
}

Mat markerMask, img, wshed;
Point prevPt(-1, -1);
Point selectedPt(-1, -1);
vector<Vec3b> lastColors;

string generateMaskExtension(int nbMaskCreated) {
    string maskExtension; 
    switch(nbMaskCreated) {
        case 0: 
            maskExtension = ".foreground";
            break;
        case 1:
            return ".background";
             break;
        case 2:
            return ".support";
            break;
    }
    return maskExtension;
}

bool isColorSelected( vector<Vec3b> lastColors, Vec3b color) {
    for (int i = 0; i < lastColors.size(); i++) {
        if (color == lastColors[i]) {
            return true;
        }
    }
    return false;
}

bool isNeightborSelected(int i, int j) {
    Vec3b downNeightbor;
    Vec3b topNeightbor; 
    Vec3b rightNeightbor;
    Vec3b leftNeightbor;
    if ( i == 0 ) {
        topNeightbor = Vec3b(0,0,0);
    } else {
        topNeightbor = wshed.at<Vec3b>(i-1,j);
    }
    
    if ( j == 0 ) {
        leftNeightbor = Vec3b(0,0,0);
    } else {
        leftNeightbor = wshed.at<Vec3b>(i,j-1);
    }
    
    if ( j > wshed.cols ) {
        rightNeightbor = Vec3b(0,0,0);
    } else {
        rightNeightbor = wshed.at<Vec3b>(i,j+1);
    }
    
    if ( i > wshed.rows ) {
        downNeightbor = Vec3b(0,0,0);
    } else {
        downNeightbor = wshed.at<Vec3b>(i+1,j-1);
    }
    
    if (isColorSelected(lastColors, downNeightbor) || 
        isColorSelected(lastColors, topNeightbor) ||
        isColorSelected(lastColors, rightNeightbor) ||
        isColorSelected(lastColors, topNeightbor)) {
        return true;
    } else {
        return false;
    }
}

static void onMouse( int event, int x, int y, int flags, void* ) {
    if( x < 0 || x >= img.cols || y < 0 || y >= img.rows )
        return;
    if( event == CV_EVENT_LBUTTONUP || !(flags & CV_EVENT_FLAG_LBUTTON) )
        prevPt = Point(-1,-1);
    else if( event == CV_EVENT_LBUTTONDOWN )
        prevPt = Point(x,y);
    else if( event == CV_EVENT_MOUSEMOVE && (flags & CV_EVENT_FLAG_LBUTTON) ) {
        Point pt(x, y);
        if( prevPt.x < 0 )
            prevPt = pt;
        line( markerMask, prevPt, pt, Scalar::all(255), 5, 8, 0 );
        line( img, prevPt, pt, Scalar::all(255), 5, 8, 0 );
        prevPt = pt;
        imshow("image", img);
    }
}

void selectAreaGUI(vector<Vec3b> lastColors, Mat initialImg, Mat modifiedImg) {
 
    if (lastColors.size() == 1) {
        for (int i = 0; i < wshed.rows; i++) {
            for (int j = 0; j < wshed.cols; j++) {
                Vec3b currentColor = initialImg.at<Vec3b>(i,j);
                if (isColorSelected(lastColors, currentColor)){
                    modifiedImg.at<Vec3b>(i,j) = Vec3b(255, 255, 255);
                } else {
                    modifiedImg.at<Vec3b>(i,j) = wshed.at<Vec3b>(i,j);
                }
            }
        }
    } else {
        for (int i = 0; i < wshed.rows; i++) {
            for (int j = 0; j < wshed.cols; j++) {
                Vec3b currentColor = initialImg.at<Vec3b>(i,j);
                if (isColorSelected(lastColors, currentColor)) {
                    modifiedImg.at<Vec3b>(i,j) = Vec3b(255, 255, 255);
                } else {
                    modifiedImg.at<Vec3b>(i,j) = wshed.at<Vec3b>(i,j);
                }
            }
        }
   }
}

static void onMouseSelectArea( int event, int x, int y, int flags, void* ) {
    if( x < 0 || x >= img.cols || y < 0 || y >= img.rows )
        return;
    else if ( event == CV_EVENT_LBUTTONDOWN && (flags & CV_EVENT_FLAG_CTRLKEY)) {
        selectedPt = Point(x,y);
        Mat wshedCopy = Mat::zeros(wshed.size(), CV_8UC3);
        Vec3b currentColor = wshed.at<Vec3b>(y,x);
        lastColors.push_back(currentColor);
        selectAreaGUI(lastColors, wshed, wshedCopy);
        imshow("watershed transform", wshedCopy);
    } else if ( event == CV_EVENT_LBUTTONDBLCLK ) {
        lastColors.clear();
        selectedPt = Point(x,y);
        Mat wshedCopy = Mat::zeros(wshed.size(), CV_8UC3);
        Vec3b currentColor = wshed.at<Vec3b>(y,x);
        lastColors.push_back(currentColor);
        selectAreaGUI(lastColors, wshed, wshedCopy);
        imshow("watershed transform", wshedCopy);
    }
}

vector<Vec3b> generateColorForWatershed(int numberOfArea) {
    vector<Vec3b> colorTab;
    for(int i = 0; i < numberOfArea; i++ ) {
        int b = theRNG().uniform(0, 255);
        int g = theRNG().uniform(0, 255);
        int r = theRNG().uniform(0, 255);
        colorTab.push_back(Vec3b((uchar)b, (uchar)g, (uchar)r));
    }
    return colorTab;
}

Mat runAndShowWatershed(Mat img0, Mat markers, int numberOfArea, 
                        vector<Vec3b> colorTab){
    
    Mat watershedImg;
    double t = (double)getTickCount();
    watershed( img0, markers );
    t = (double)getTickCount() - t;
    printf( "execution time = %gms\n", t*1000./getTickFrequency() );

    watershedImg = Mat::zeros(markers.size(), CV_8UC3);
    watershedImg = Scalar::all(0);
    // paint the watershed image
    for( int i = 0; i < markers.rows; i++ ) {
        for( int j = 0; j < markers.cols; j++ ) {
            int index = markers.at<int>(i,j);
            if( index == -1 )
                watershedImg.at<Vec3b>(i,j) = Vec3b(255,255,255);
            else if( index <= 0 || index > numberOfArea )
                watershedImg.at<Vec3b>(i,j) = Vec3b(255,255,255);
            else
                watershedImg.at<Vec3b>(i,j) = colorTab[index - 1];
        }
    }
    return watershedImg;
}

void createMaskFromWatershed(string outputMaskFileName, int nbMaskCreated) {
    
    string maskExtension = generateMaskExtension(nbMaskCreated);
    string outputMaskName = outputMaskFileName + ".mask";
    ofstream outputStream(outputMaskName.c_str());
    
    if (!outputStream) {
        cout << "issue while creating the output file";
    }
    
    outputStream << wshed.rows << " " << wshed.cols << "\n" ;
    
    Mat mask = Mat::zeros(wshed.rows, wshed.cols, CV_8UC1);
    for (int i = 0; i < wshed.rows; i++) {
        for (int j = 0 ; j < wshed.cols; j++) {
            Vec3b currentColor = wshed.at<Vec3b>(i,j);
            if (currentColor == Vec3b(255,255,255)) {
                if (isNeightborSelected(i,j)) {
                    mask.at<uchar>(i,j) = 255;
                    outputStream << 1;
                } else {
                     mask.at<uchar>(i,j) = 0;
                     outputStream << 0;
                }
            } else {
                if (isColorSelected(lastColors, currentColor)) {
                    mask.at<uchar>(i,j) = 255;
                    outputStream << 1;
                } else {
                    mask.at<uchar>(i,j) = 0;
                    outputStream << 0;
                }
            }
         }
         outputStream << "\n";
    }
    imwrite(outputMaskFileName + maskExtension + ".jpg", mask);
    imwrite(outputMaskFileName + maskExtension + ".pgm", mask);
    namedWindow("mask" + maskExtension);
    imshow("mask" + maskExtension, mask);
}

int main( int argc, char** argv ) {
    if ( argc < 3) {
        cout << "Usage: watershedUserAssistMaskCreation <image_name> <mask_name>\n";
    }
    string filename = argv[1] ;
    string outputMaskFileName = argv[2];
    
    Mat img0 = imread(filename, 1), imgGray;
    
    if( img0.empty() ) {
        cout << "Couldn't open image " << filename 
             << ". Usage: watershedUserAssistMaskCreation <image_name> <mask_name>\n";
        return 0;
    }
   
    help();
    namedWindow( "image", 1 );

    img0.copyTo(img);
    cvtColor(img, markerMask, CV_BGR2GRAY);
    cvtColor(markerMask, imgGray, CV_GRAY2BGR);
    markerMask = Scalar::all(0);
    imshow( "image", img );
    setMouseCallback( "image", onMouse, 0 );

    for(;;) {
        int c = waitKey(0);

        if( (char)c == 27 )
            break;

        if( (char)c == 'r' ) {
            markerMask = Scalar::all(0);
            img0.copyTo(img);
            imshow( "image", img );
        }

        if( (char)c == 'w' || (char)c == ' ' ) {
            int i, j, compCount = 0;
            vector<vector<Point> > contours;
            vector<Vec4i> hierarchy;

            findContours(markerMask, contours, hierarchy, CV_RETR_CCOMP, CV_CHAIN_APPROX_SIMPLE);

            if( contours.empty() )
                continue;
            Mat markers(markerMask.size(), CV_32S);
            markers = Scalar::all(0);
            int idx = 0;
            for( ; idx >= 0; idx = hierarchy[idx][0], compCount++ )
                drawContours(markers, contours, idx, Scalar::all(compCount+1), -1, 8, hierarchy, INT_MAX);

            if( compCount == 0 )
                continue;
            
            vector<Vec3b> colorTab = generateColorForWatershed(compCount);
            wshed = runAndShowWatershed(img0, markers, compCount, colorTab);
            imshow( "watershed transform", wshed );
            setMouseCallback("watershed transform", onMouseSelectArea, 0 );
            
            int nbMaskCreated =0;
            for (;;) {
                int k = waitKey(0);
                if( (char)k == 's') {
                    createMaskFromWatershed(outputMaskFileName, nbMaskCreated);
                    nbMaskCreated++;
                }
                if( (char)k == 27 )
                    break;
            }
            break;
        }
        
    }
    return 0;
}
