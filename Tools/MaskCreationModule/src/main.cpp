/* 
 * File:   main.cpp
 * Author: athollon
 *
 * Created on 23 février 2014, 19:21
 */

#include <cstdlib>
#include "MaskCreationModule.hpp"

using namespace std;

int main(int argc, char** argv) {
   
    string outputMaskPath = "";
    string outputMaskForClassifierPath = "./classifier/";
    string inputImagePath = "";
    vector<string> imageName;
    imageName.push_back("temple0001");
    imageName.push_back("temple0003");
    string extension = ".png";
    
    MaskCreationModule maskCreationModule(inputImagePath, outputMaskPath, outputMaskForClassifierPath);
    maskCreationModule.processUserAssistMaskCreation(imageName, extension);
    
}

