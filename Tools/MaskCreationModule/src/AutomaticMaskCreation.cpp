/* 
 * File:   AutomaticMaskCreation.cpp
 * Author: athollon
 * 
 * Created on 3 mars 2014, 22:05
 */

#include "AutomaticMaskCreation.hpp"
#include "HistClassifier.hpp"

using namespace std;
using namespace cv;

AutomaticMaskCreation::AutomaticMaskCreation() {
}

AutomaticMaskCreation::AutomaticMaskCreation(const AutomaticMaskCreation& orig) {
}

AutomaticMaskCreation::~AutomaticMaskCreation() {
}


/**
 * 
 * @param imagesName the root name of each image which need to be classify
 *      example : 0000001
 * @param imagesForClassifier : the root name of each image used previously with
 *         the userAssistMaskCreation
 * @param extension : the extension of the files
 * @param imagePath : the path of the images /input/
 * @param maskOutputPath : the output for the mask
 * @param maskForClassifierPath :  the path of the mask used by the classifier
 */
void AutomaticMaskCreation::processAutomaticMaskCreation(vector<string> imagesName, vector<string> imagesForClassifier, string extension, 
                                string imagePath, string maskOutputPath, string maskForClassifierPath) {
    
    
    // First initialize the classifier with all the list of images on which was run
    // the userAssistMaskCreation   
    
    HistClassifier classifier(imagesForClassifier, imagePath, maskForClassifierPath, extension);
    
    ColorimetricSpace* hsvColorSpace = new HSVColorimetricSpace();
    classifier.setColorimetricSpace(hsvColorSpace);
        
    vector<int> channels;
    channels.push_back(1);
    channels.push_back(2);
    
    classifier.initClassifierHist(channels);
    for (int i = 0; i < imagesName.size(); i++) {
        string currentImagePath =  imagePath + imagesName[i] + extension;        
        classifier.getImageMask(currentImagePath, channels, maskOutputPath, imagesName[i]);
    }
    
    destroyAllWindows();
}
