/* 
 * File:   MaskCreationModule.cpp
 * Author: athollon
 * 
 * Created on 28 février 2014, 12:10
 *  */
#include "MaskCreationModule.hpp"

MaskCreationModule::MaskCreationModule() {
}

MaskCreationModule::MaskCreationModule
        ( std::string inputPath, std::string maskOutputPath, std::string maskForClassifierPath) :
         m_maskForClassifierPath(maskForClassifierPath), m_inputPath(inputPath), m_maskOutputPath(maskOutputPath) {
    
}
        
void MaskCreationModule::processUserAssistMaskCreation(std::vector<std::string> imagesName, std::string formatOfImage) {
    UserAssistMaskCreation userAssistMaskCreation;
    userAssistMaskCreation.processWatershedUserAssistMaskCreation(imagesName, formatOfImage,
                                           m_inputPath, m_maskOutputPath,
                                           m_maskForClassifierPath );
}                               

void MaskCreationModule::processAutomaticMaskCreation(std::vector<std::string> imagesName, 
        std::vector<std::string> imgsForClassifierName, std::string formatOfImage) {

    AutomaticMaskCreation automaticMaskCreation;
    automaticMaskCreation.processAutomaticMaskCreation(imagesName, imgsForClassifierName, formatOfImage, m_inputPath,
                        m_maskOutputPath, m_maskForClassifierPath);

}

MaskCreationModule::MaskCreationModule(const MaskCreationModule& orig) {
}

MaskCreationModule::~MaskCreationModule() {
}

void MaskCreationModule::setInputPath(std::string inputPath) {
    m_inputPath = inputPath;
}

void MaskCreationModule::setOutputPath(std::string outputPath) {
    m_maskOutputPath = outputPath;
}

void MaskCreationModule::setMaskForClassifierPath(std::string maskForClassifierPath) {
    m_maskForClassifierPath = maskForClassifierPath;
}