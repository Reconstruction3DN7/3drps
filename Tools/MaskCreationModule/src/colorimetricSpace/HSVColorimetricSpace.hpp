/* 
 * File:   HSVColorimetricSpace.hpp
 * Author: athollon
 *
 * Created on 24 février 2014, 15:53
 */

#ifndef HSVCOLORIMETRICSPACE_HPP
#define	HSVCOLORIMETRICSPACE_HPP

#include "colorimetricSpace/ColorimetricSpace.hpp"


class HSVColorimetricSpace : public ColorimetricSpace {
public:
    
    HSVColorimetricSpace();
    HSVColorimetricSpace(const HSVColorimetricSpace& orig);
   
    virtual ~HSVColorimetricSpace();

    /**
     * @param initialImg : the initial image which is needed to be converted
     *                     we suppose it is RGB image
     * @return : a matrix containing the value of the initial image in the 
     *                  colorimetric space
     */
    virtual cv::Mat convertIntoCurrentColorimetricSpace(cv::Mat initialImg);

    
private:
    
};

#endif	/* HSVCOLORIMETRICSPACE_HPP */

