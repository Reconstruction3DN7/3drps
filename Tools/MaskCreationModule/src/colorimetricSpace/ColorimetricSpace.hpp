/* 
 * File:   ColorimetricSpace.hpp
 * Author: athollon
 *
 * Created on 24 février 2014, 11:05
 */

#ifndef COLORIMETRICSPACE_HPP
#define	COLORIMETRICSPACE_HPP

#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <vector>
#include <string>
#include <iostream>

class ColorimetricSpace {

public:
    
    ColorimetricSpace();
    
    ColorimetricSpace(int nbOfChannel);
    
    ColorimetricSpace(const ColorimetricSpace& orig);
    
    virtual ~ColorimetricSpace();
    
    /**
     * @param initialImg : the initial image which is needed to be converted
     *                     we suppose it is RGB image
     * @return : a matrix containing the value of the initial image in the 
     *                  colorimetric space
     */
    virtual cv::Mat convertIntoCurrentColorimetricSpace(cv::Mat initialImg);
    
    /**Show the image in the associated colorimeric space;
     * 
     * @param image
     */
    void showImgInColorimetricSpace(cv::Mat image);
    
    
    int getNbOfChannels();
    std::vector<std::string> getChannelsName();
    const float** getAllChannelsRange();
    std::vector<int> getBinsValue();

    
protected:

    int m_nbOfChannels;
    const float** m_allChannelsRange;
    std::vector<std::string> m_channelsName;
    
    std::vector<int> m_binList;

};

#endif	/* COLORIMETRICSPACE_HPP */

