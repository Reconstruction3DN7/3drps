/* 
 * File:   MaskCreationModule.hpp
 * Author: athollon
 *
 * Created on 28 février 2014, 12:10
 */

#ifndef MASKCREATIONMODULE_HPP
#define	MASKCREATIONMODULE_HPP
#include <iostream>
#include <vector>
#include <string>
#include "UserAssistMaskCreation.hpp"
#include "AutomaticMaskCreation.hpp"

class MaskCreationModule {
public:
    MaskCreationModule();
    
    MaskCreationModule(std::string inputPath, std::string maskOutputPath, std::string m_maskForClassifierPath);
    
    MaskCreationModule(const MaskCreationModule& orig);
    
    std::string getInputPath();
    
    std::string getOutputPath();
    
    std::string getMaskForClassifierPath();
    
    void setInputPath(std::string inputPath);
    
    void setOutputPath(std::string outputPath);
    
    void setMaskForClassifierPath(std::string maskForClassifierPath);
    
    virtual ~MaskCreationModule();
    
    /**
     * Process the user assist mask Creation
     * @param imagesName : the name of the images which will be used by the
     *                  user to define foreground, background and support Mask
     *                  Must be 00000000, 00000001 for example 
     * @param formatOfImage : the extension used: .jpg, .png
     */
    void processUserAssistMaskCreation(std::vector<std::string> imagesName, std::string formatOfImage);
    
    void processAutomaticMaskCreation(std::vector<std::string> imagesName, std::vector<std::string>  imgsForClassifierName, std::string formatOfImage);
        
private:
    
    std::string m_inputPath; // The path of the pictures which need a mask
    std::string m_maskOutputPath; // The path of the folder where we save final mask
    std::string m_maskForClassifierPath; // the path of the folder where the mask for classifier are saved
    
};

#endif	/* MASKCREATIONMODULE_HPP */

