/* 
 * File:   ColorimetricSpace.cpp
 * Author: athollon
 * 
 * Created on 24 février 2014, 11:05
 */

#include "colorimetricSpace/ColorimetricSpace.hpp"

using namespace cv;
using namespace std;

ColorimetricSpace::ColorimetricSpace() {
    cout << "default construct used";
}

ColorimetricSpace::ColorimetricSpace(int nbOfChannels) : m_nbOfChannels(nbOfChannels) {
    cout << "nb of Channel init";
}

ColorimetricSpace::ColorimetricSpace(const ColorimetricSpace& orig) {
}

ColorimetricSpace::~ColorimetricSpace() {
}


void ColorimetricSpace::showImgInColorimetricSpace(cv::Mat img) {
    vector<Mat> channels;
    Mat imgInColorimetricSpace = convertIntoCurrentColorimetricSpace(img);
    cout << m_nbOfChannels;
    split(imgInColorimetricSpace,channels);
    for (int i=0; i < m_nbOfChannels; i++) {
        namedWindow(m_channelsName[i],1);
        imshow(m_channelsName[i], channels[i]);
    }
    waitKey();
}

cv::Mat ColorimetricSpace::convertIntoCurrentColorimetricSpace(cv::Mat initialImg) {

}

int ColorimetricSpace::getNbOfChannels() {
    return m_nbOfChannels;
}

std::vector<std::string> ColorimetricSpace::getChannelsName() {
    return m_channelsName;
}

std::vector<int> ColorimetricSpace::getBinsValue() {
    return m_binList;
}

const float** ColorimetricSpace::getAllChannelsRange() {
    return m_allChannelsRange;
}


