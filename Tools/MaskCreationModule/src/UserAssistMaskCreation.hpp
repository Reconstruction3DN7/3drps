/* 
 * File:   UserAssistMaskCreation.hpp
 * Author: athollon
 *
 * Created on 1 mars 2014, 10:51
 */

#ifndef USERASSISTMASKCREATION_HPP
#define	USERASSISTMASKCREATION_HPP
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "stdio.h"
#include <iostream>
#include <fstream>
#include "colorimetricSpace/ColorimetricSpace.hpp"

class UserAssistMaskCreation {
public:
    
    UserAssistMaskCreation();
    
    
    UserAssistMaskCreation(const UserAssistMaskCreation& orig);
    
    virtual ~UserAssistMaskCreation();
    
     /**
     * 
     * @param imageName
     * @param extension
     * @param imagePath
     * @param maskOutputPath
     * @param maskForClassifierOutputPath
     */
    void processWatershedUserAssistMaskCreation(std::vector<std::string> imagesName, std::string extension,
                                           std::string imagePath, std::string maskOutputPath,
                                           std::string maskForClassifierOutputPath );
    
    static void onMouse(int event, int x, int y, int flags, void* param);   

    static void onMouseSelectArea(int event, int x, int y, int flags, void* param);
    
     /**
     *  Event listener based on mouse event on the image. Used to create marker
     * @param prevPt : last Point selected
     * @param img: image on which the event listener is used
     * @param event : event on the figure on which we create markers
     * @param x : the position of the mouse .x
     * @param y : the position of the mouse .y
     * @param flags : flags raised during the event
     * @param 
     */
     void onMouseUAMC(int event, int x, int y, int flags );
    
         
    /**
     *  Event listener based on mouse event on the wshed image. Used to select an Area
     * @param event : event on the figure on which we create markers
     * @param x : the position of the mouse .x
     * @param y : the position of the mouse .y
     * @param flags : flags raised during the event
     */
     void onMouseSelectAreaUAMC(int event, int x, int y, int flags);   
     
private:
    
    cv::Mat m_img;
    cv::Point m_prevPt;
    cv::Mat m_wshed;
    cv::Mat m_markerMask;
    std::vector<cv::Vec3b> m_lastColors;
    
    cv::Mat getImg();
    cv::Point getPrevPt();
    
    /**
     * Print the help For user;
     */
    static void help();

    /**
     * Generate a mask extension for classifier mask
     * @param nbMaskCreated :  number of mask created during the loop
     *                          if 0 : .foreground is return
     *                          if 1 : .background is return
     *                          if 2 : .support is return
     * @return 
     */
    std::string generateMaskExtension(int nbMaskCreated);
    
    /**
     *  Return true if color is in the lastColors list
     * @param lastColors: list of the color which were selected by the user
     * @param color : current Color
     * @return 
     */
    bool isColorSelected( std::vector<cv::Vec3b> lastColors, cv::Vec3b color);
    
    /**
     * Check if pixel around the pixel(i,j) is selected, ie is white
     * @param whsed: the watershed Matrix where the check is done
     * @param i 
     * @param j
     * @return 
     */
    bool isNeightborSelected(cv::Mat wshed, int i, int j);
    
     /**
     * Used to select area in order to generate mask
     * @param lastColors : the selected colors
     * @param initialImg : the initial image 
     * @param modifiedImg : the modified image
     */
    void selectAreaGUI( std::vector<cv::Vec3b> lastColors, cv::Mat initialImg, cv::Mat modifiedImg);
    
    /**
     * Generate a color for each area
     * @param numberOfArea
     * @return 
     */
    std::vector<cv::Vec3b> generateColorForWatershed(int numberOfArea);
    
    /**
     * Run watershed based on marker used by the user
     * @param img0 : the initial image
     * @param markers : the markers defined by the user
     * @param numberOfArea : the number of Area found by find contours
     * @param colorTab : the color for each area
     * @return 
     */
    cv::Mat runAndShowWatershed(cv::Mat img0, cv::Mat markers, int numberOfArea, 
                        std::vector<cv::Vec3b> colorTab);
    
    /**
     * Create mask from the watershed
     * @parm wshed: image of the watershed result
     * @param maskFilePath : path of the file where the mask should be saved (pmvs)
     * @param maskForClassifierFilePath : path of the file where the mask should be saved for classifier
     * @param nbMaskCreated : the number of mask already created for the current image
     */
    void createMaskFromWatershed(cv::Mat wshed, std::string maskFilePath, std::string maskForClassifierFilePath, int nbMaskCreated);
    
    /**
     * 
     * @param imageName : name of the image on which we need a watershed ex: temple00001
     * @param extension : extension of the image ex: .jpg, .png
     * @param imagePath : path where we could find the image
     * @param maskOutputPath : path where the mask should be saved
     * @param maskForClassifierOutputPath : path where the masks used for classifier should be written
     */
    void processWatershedUserAssistMaskCreation(std::string imageName, std::string extension,
                                std::string imagePath, std::string maskOutputPath, 
                                std::string maskForClassifierOutputPath );
    
    /**
     */

};

#endif	/* USERASSISTMASKCREATION_HPP */

