/* 
 * File:   HistClassifier.hpp
 * Author: athollon
 *
 * Created on 23 février 2014, 13:03
 */

#ifndef HISTCLASSIFIER_HPP
#define	HISTCLASSIFIER_HPP

#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "vector"
#include "string"
#include "stdio.h"
#include <iostream>
#include "../colorimetricSpace/ColorimetricSpace.hpp"
#include "../colorimetricSpace/HSVColorimetricSpace.hpp"
#include <fstream>

class HistClassifier {
    
public:
    
    HistClassifier();
    HistClassifier(std::vector<std::string> imageNames, std::string imgPath, std::string masksPath, std::string extension);
    HistClassifier(const HistClassifier& orig);
    virtual ~HistClassifier();
    
    void showImgAndMask(int imageNumber);
    
    cv::MatND getHistogramOfSpecifiedPicture(int imageNumber, std::vector<int> selectedChannelsId, int maskType, ColorimetricSpace* colorimetricSpace);
    
    /**
     * 
     * @param imageNumber : id of the image to use in the dataset
     * @param selectedChannelsId : channel to select to compute the histogram
     *                          example: to get channel h and s of the HSVCOlorspace
     *                          we use [0,1]
     * @param maskType : type of the mask to be used for computing Histogram
     *                  0 : foreground
     *                  1 : background
     *                  2 : support
     * @return Histogram of the unmask area on the selected Channel for the selected
               picture
     */
    
    cv::MatND getHistogramOfSpecifiedPicture(int imageNumber, std::vector<int> selectedChannelsId, int maskType);
    /**
     * Set the colorimetricSpace with the param colorimetricSpace
     * @param colorimetricSpace
     */
    void setColorimetricSpace(ColorimetricSpace* colorimetricSpace);
    
    void showChannelsOfImgInColorimetricSpace(int imageNumber);
    
    /**
     * 
     * @param histogram The histogram to be printed
     * @param channelsId : channels used to define the histogram
     */
    void showHistogram(cv::MatND histogram, std::vector<int> channelsId, ColorimetricSpace* colorimetricSpace);
    
    void showHistogram(cv::MatND histogram, std::vector<int> channelsId);
    
    /**return the backProjection
     * 
     * @param hist : histogram used to do the backprojection
     * @param colorimetricSpace : colorimetricSpace from which histogram hist was computed
     * @return 
     */
    
    cv::MatND getBackProjection(cv::MatND hist,cv::Mat image, std::vector<int> selectedChannel, ColorimetricSpace* colorimetricSpace);
    
    /**
     * Init the classifier's histograms (foreground, background and support)
     */
    void initClassifierHist(std::vector<int> selectedChannelsId);
    
    void getImageMask(std::string currentImagePath, std::vector<int> selectedChannel, std::string outputPath, std::string imageName);
        
private:

    ///////////////////////////////////////////////////////////////////////////
    //////////////  FUNCTIONS TO INITIALIZE THE DATASET PICTURES //////////////
    ///////////////////////////////////////////////////////////////////////////     
    /**
     * 
     * @param imgName : Name of the image from which we will use mask
     * @return list of the name of the masks (foreground, background and support)
     */
    std::vector<std::string> generateImgAndMaskName(std::string imgName, std::string imgPath, std::string maskPath, std::string extension);
    
    /**
     * Initialisation of the dataset Data
     * @param imgNames : Name of the images which will be used as dataset.
     *        backgroundMask must be written with imgName.background.jpg
     *        foregroundMask must be written with imgName.foreground.jpg
     *        supportMask must be written with imgName.support.jpg
     * @return : 0 if the acquisition worked, else 1;
     */
    int initializeDatasetData(std::vector<std::string> imgNames, std::string imgPath, std::string maskPath, std::string extension);
  
    
    int initializeDataset(std::vector<std::string> imagesName);
    
    void initClassifierHist(std::vector<int> selectedChannelsId, int maskType);
    
    void findMarkerAndComputeWatershed(cv::Mat binForegroundBP,
                                        cv::Mat binBackgroundBP, cv::Mat binSupportBP, cv::Mat initialImage,
                                        std::string outputMaskPath, std::string imageName);
    
    ///////////////////////////////////////////////////////////////////////////
    /////////////////    FUNCTIONS FOR COLORIMETRIC SPACE     /////////////////
    ///////////////////////////////////////////////////////////////////////////
    
    //datasetImg : Images used to generate the classifier hist
    std::vector<cv::Mat> m_datasetImg; 
    
    //datasetSize: Number of images used to define the dataset
    int m_datasetSize; 
    
    //maskForDatasetImg : Masks needed for select the right area for each image
    //of the dataset
    std::vector<cv::Mat> m_backgroundMaskForDatasetImg;
    std::vector<cv::Mat> m_foregroundMaskforDatasetImg;
    std::vector<cv::Mat> m_supportMaskForDatasetImg;
    
    //classifierHist: Contains the data about the histogram of each class we 
    //need to use, ie. foreground, background and support
    std::vector<cv::MatND> m_classifierHists;
    
    ColorimetricSpace* m_colorimetricSpace;
};

#endif	/* HISTCLASSIFIER_HPP */

