/**
* @file omvgPmvsCallback.h
* @brief File for the declaration of the omvgPmvsCallback
*  class
* @date 25/02/2014
*/

#ifndef OMVGPMVSCALLBACK_H
#define OMVGPMVSCALLBACK_H

#include <string>

/**
* @class omvgPmvsCallback
* @brief Abstract class for receiving callbacks from an
*  omvgPmvsReconstructionEngine
*/
class omvgPmvsCallback {
public:
    /**
     * @brief This is an enumeration of all status flags possible when using
     *  omvgPmvsReconstructionEngine
     */
	enum ReconstructionStatus
	{
		/// Not doing anything
        IDLE,
        /// Computing camera focals
        CREATE_LISTS,
        /// Computing images features
        COMPUTE_FEATURES,
        /// Matching pairs of pictures
        MATCHING_PAIRS,
        /// Filtering putative matches
        FILTERING_MATCHES,
        /// Performing sparse reconstruction
        SFM,
        /// Performing dense reconstruction
        DENSE_RECONSTRUCTION,
        /// Dense reconstruction is finished
        DENSE_RECONSTRUCTION_FINISHED
	};

    /**
     * @brief This is an enumeration of all error flags possible when using
     *  omvgPmvsReconstructionEngine
     */
    enum ReconstructionError
    {
		/// Everything's fine
        NONE,
        /// Sensor db not found and no default focal provided
        SENSORDBASE_INVALID_NO_FOCAL,
        /// Couldn't create lists.txt in outdir
        LISTSTXT_NOT_OPENED,
        /// Not enough valid images to process
        EMPTY_IMAGE_LIST,
        /// SfMEngine did not complete its task
        FAILED_SFM,
        /// PMVS returned EXIT_FAILURE
        FAILED_DENSE_RECONSTRUCTION
    };

    /**
     * @brief onStatusChange is called when the state of the reconstruction
     *  changes (see ReconstructionStatus)
     * @param status New status of the reconstruction
     */

    virtual void onStatusChange(ReconstructionStatus status) {}

    /**
     * @brief onError is called when an error happens
     * @param error Error flag (see ReconstructionError)
     */
    virtual void onError(ReconstructionError error) {}

    /**
     * @brief onCreateListCameraModel is called when an image is processed by
     *  createList()
     * @param imageFileName     Name of the picture
     * @param cameraRecognized  Whether the camera was recognized
     * @param focal             Focal detected. defaults to 1 or sFocalLength.
     * @param width             Width of the picture.
     * @param height            Height of the picture.
     * @param camName           Name of the picture's detected camera.
     * @param camModel          Model of the picture's detected camera.
     */
    virtual void onCreateListCameraModel(std::string imageFileName,
                                         bool cameraRecognized,
                                         double focal = -1,
                                         size_t width = -1,
                                         size_t height = -1,
                                         std::string camName = "",
                                         std::string camModel = "") {}

    /**
     * @brief onComputeMatchesFeaturesExtracted is called each time we extract an
     *  image's features
     * @param imageFileName     Name of the picture
     * @param imageNumber       Number of the picture in the set
     * @param totalImagesNumber Number of pictures in the set (if we have
     *  imageNumber == totalImagesNumber we are at the end of the set)
     */
    virtual void onComputeMatchesFeaturesExtracted(std::string imageFileName,
                                                   int imageNumber,
                                                   int totalImagesNumber) {}

    // TODO : initial pair choice
    // TODO : each step of sfm
    // TODO : dense reconstruction steps
};

#endif // OMVGPMVSCALLBACK
