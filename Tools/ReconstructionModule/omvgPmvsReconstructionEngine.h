/**
* @file omvgPmvsReconstructionEngine.h
* @brief File for the declaration of the omvgPmvsReconstructionEngine
*  class
* @date 05/02/2014
*/

// Parts Copyright (c) 2012, 2013 Pierre MOULON.

#ifndef OMVGPMVSRECONSTRUCTIONENGINE_H
#define OMVGPMVSRECONSTRUCTIONENGINE_H

#include "omvgPmvsCallback.h"

// Includes for createList()
#include "openMVG/exif_IO/exif_IO_openExif.hpp"
#include "third_party/stlplus3/filesystemSimplified/file_system.hpp"
#include "ParseDatabase.hpp"
#include "openMVG/image/image.hpp"

// Includes for computeMatches()
#include "openMVG/features/features.hpp"
#include "software/SfM/ImageCollectionMatcher_AllInMemory.hpp"
#include "software/SfM/ImageCollectionGeometricFilter.hpp"
#include "software/SfM/ImageCollection_F_ACRobust.hpp"
#include "pairwiseAdjacencyDisplay.hpp"
#include "software/SfM/SfMIOHelper.hpp"
#include "openMVG/matching/matcher_brute_force.hpp"
#include "openMVG/matching/matcher_kdtree_flann.hpp"
#include "openMVG/matching/indMatch_utils.hpp"
/// Feature detector and descriptor interface
#include "patented/sift/SIFT.hpp"

// Includes for incrementalSfM()
#include "SfMIncrementalEngine.hpp"

#include <iostream>
#include <fstream>
#include <memory>
#include <string>
#include <vector>
#include <cstdlib>
#include <iostream>
#include <iterator>

/**
* @class omvgPmvsReconstructionEngine
* @brief Reconstructs a point cloud from a set of images
*/
class omvgPmvsReconstructionEngine
{
public:
    omvgPmvsReconstructionEngine(const std::string sInputDirectory,
                                 const std::string sOutputDirectory,
                                 const std::string sSensorWidthDatabase,
                                 const int sFocalLength = -1);

    ~omvgPmvsReconstructionEngine();

    virtual bool Process();
    // Set up a callback
    void setCallback(omvgPmvsCallback& callback);
    // Create the list of focals for the images
    bool createList();
    // Compute matches for each pair of images
    bool computeMatches();
    // Perform incremental SfM for sparse reconstruction on the dataset
    bool incrementalSfm();
    // Perform dense reconstruction with PMVS on the dataset
    bool denseReconstruction();

    // Nearest neighbor ratio for putative descriptor matches
    void setNearestNeighborDistanceRatio(float fDistRatio);
    float getNearestNeighborDistanceRatio();
    // Input directory, containing the pictures
    void setInputDirectory(std::string sInputDirectory);
    std::string getInputDirectory();
    // Output directory, where project files will be saved
    void setOutputDirectory(std::string sOutputDirectory);
    std::string getOutputDirectory();
    // Path to pmvs executable (should be removed if and when pmvs is used as a library)
    void setPathToPmvs(std::string sPathToPmvs);
    std::string getPathToPmvs();
    // Default focal length if the camera model isn't recognized
    void setDefaultFocalLength(int sFocalLength);
    int getDefaultFocalLength();
    // Path to sensor width database (cameraGenerated.txt)
    void setSensorWidthDatabasePath(std::string sSensorWidthDatabasePath);
    std::string getSensorWidthDatabasePath();
    // octminus -1 option of SIFT
    bool getSiftFirstOctaveMinusOne() const;
    void setSiftFirstOctaveMinusOne(bool bOctMinus1);
    // Peak treshold option of SIFT
    float getPeakThreshold() const;
    void setPeakThreshold(float dPeakThreshold);
    // Whether to refine principal point and radial distorsion
    bool getRefinePPandDisto() const;
    void setRefinePPandDisto(bool bRefinePPandDisto);

private:
    std::string _sInputDirectory;		// Directory that contains input pics
    std::string _sOutputDirectory;		// Output Directory
    std::string _sMatchesDirectory;		// Directory that will contain pairwise matches
    std::string _sSensorWidthDatabase;	// CameraGenerated.txt file
    std::string _sPathToPmvs;			// Path to the PMVS executable
    int _sFocalLength;					// Focal length for all files if available (in mm)

    omvgPmvsCallback * _callback;       // Callback to handle states and errors

    float _fDistRatio;					// Nearest Neighbor distance ratio, def=0.6f
    bool _bOctMinus1;					// Whether to use octave -1 option of SIFT
    float _dPeakThreshold;				// Peak threshold option of sift, def=0.04f
    bool _bRefinePPandDisto;			// Whether to refine principal point and radial distorsion
};

#endif
