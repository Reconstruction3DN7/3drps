/**
* @file omvgPmvsReconstructionEngine.cpp
* @brief File for the definition of the omvgPmvsReconstructionEngine
*  class' methods
* @date 05/02/2014
*/

// Parts Copyright (c) 2012, 2013 Pierre MOULON.

#include "omvgPmvsReconstructionEngine.h"

/**
 * Basic constructor for the Reconstruction engine.
 * @param sInputDirectory Path of input pictures. Must exist.
 * @param sOutputDirectory Desired path for output.
 * @param sSensorWidthDatabase Path of sensor database (like
 * 	CameraGenerated.txt from openMVG)
 * @param sFocalLength Default focal length for the pictures.
 * 	Defaulted as -1.
 */
omvgPmvsReconstructionEngine::omvgPmvsReconstructionEngine(const std::string sInputDirectory,
                                                           const std::string sOutputDirectory,
                                                           const std::string sSensorWidthDatabase,
                                                           const int sFocalLength)
    : _sInputDirectory(sInputDirectory),
      _sOutputDirectory(sOutputDirectory),
      _sMatchesDirectory(stlplus::folder_append_separator(_sOutputDirectory) + "matches"),
      _sSensorWidthDatabase(sSensorWidthDatabase),
      _sFocalLength(sFocalLength)
{
    _fDistRatio = .6f;
    _bOctMinus1 = false;
    _dPeakThreshold = .04f;
    // For the SfM part
    _bRefinePPandDisto = true;
    // For the dense reconstruction part
    _sPathToPmvs = "./pmvs2";

    _callback = new omvgPmvsCallback();
}

/**
 * Sample destructor for omvgPmvsReconstructionEngine.
 */
omvgPmvsReconstructionEngine::~omvgPmvsReconstructionEngine()
{
}

/**
 * Executes the OpenMVG and PMVS routines for reconstruction (SfM sparse
 * reconstruction + dense reconstruction).
 * @return Whether the algorithm has passed (true) or failed (false).
 */
bool omvgPmvsReconstructionEngine::Process()
{
    bool bOk = true;

    // If necessary, create the output directory
    if (!stlplus::folder_exists(_sOutputDirectory))
    {
        stlplus::folder_create(_sOutputDirectory);
    }

    // Create the list of focals for the pictures
    bOk = this->createList();

    // Compute matches between each pair of pictures
    if (bOk)
        bOk = this->computeMatches();

    // Compute a sparse reconstruction of the scene
    if (bOk)
        bOk = this->incrementalSfm();

    // Call PMVS for dense reconstruction
    if (bOk)
        bOk = this->denseReconstruction();

    return bOk;
}


/**
 * Computes the list of focal length for the input pictures, stores it in
 * _sOutputDirectory/matches/lists.txt
 * @return Whether the algorithm has passed (true) or failed (false).
 */
bool omvgPmvsReconstructionEngine::createList()
{
    bool bOk = true;

    // If we don't have a default focal length nor database, fail.
    if (!stlplus::file_exists(_sSensorWidthDatabase) && (_sFocalLength == -1)) {

        _callback->onError(omvgPmvsCallback::SENSORDBASE_INVALID_NO_FOCAL);
        bOk = false;
    }
    else
    {
        // If necessary, create the matches directory
        if (!stlplus::folder_exists(_sMatchesDirectory))
        {
            stlplus::folder_create(_sMatchesDirectory);
        }

        // Write lists.txt
        std::vector<std::string> vec_image = stlplus::folder_all( _sInputDirectory );
        std::ofstream listTXT( stlplus::create_filespec( _sMatchesDirectory,
                                                         "lists.txt" ).c_str() );

        if( listTXT )
        {
            std::sort(vec_image.begin(),vec_image.end());
            for( std::vector<std::string>::const_iterator iter_image = vec_image.begin();
                 iter_image != vec_image.end();
                 iter_image++ )
            {
                // Read metadata to fill width, height and _sFocalLength.
                std::string sImageFilename = stlplus::create_filespec( _sInputDirectory, *iter_image );

                size_t width = -1;
                size_t height = -1;

                std::auto_ptr<Exif_IO> exifReader ( new Exif_IO_OpenExif() );
                exifReader->open( sImageFilename );

                std::ostringstream os;

                // If the image does not have metadata
                if ( !exifReader->doesHaveExifInfo() || _sFocalLength != -1 )
                {
                    Image<unsigned char> image;
                    if (openMVG::ReadImage( sImageFilename.c_str(), &image))
                    {
                        width = image.Width();
                        height = image.Height();
                    }
                    else
                    {
                        Image<RGBColor> imageRGB;
                        if (openMVG::ReadImage( sImageFilename.c_str(), &imageRGB))
                        {
                            width = imageRGB.Width();
                            height = imageRGB.Height();
                        }
                        else
                        {
                            continue; // Image is not considered, cannot be read
                        }
                    }

                    if ( _sFocalLength == -1)
                    {
                        os << *iter_image << ";" << width << ";" << height << std::endl;
                    }
                    else
                    {
                        os << *iter_image << ";" << width << ";" << height << ";"
                           << _sFocalLength << ";" << 0 << ";" << width/2.0 << ";"
                           << 0 << ";" << _sFocalLength << ";" << height/2.0 << ";"
                           << 0 << ";" << 0 << ";" << 1 << std::endl;
                    }

                    _callback->onCreateListCameraModel(sImageFilename,
                                                       false,
                                                       _sFocalLength,
                                                       width,
                                                       height);
                }
                else // If image contains meta data
                {
                    double focal = _sFocalLength;
                    width = exifReader->getWidth();
                    height = exifReader->getHeight();
                    std::string sCamName = exifReader->getBrand();
                    std::string sCamModel = exifReader->getModel();

                    std::vector<Datasheet> vec_database;
                    Datasheet datasheet;
                    if ( parseDatabase( _sSensorWidthDatabase, vec_database ) )
                    {
                        // If the camera model is recognized : compute its approximate focal length
                        if ( getInfo(sCamName,sCamModel,vec_database,datasheet ) )
                        {
                            double ccdw = datasheet._sensorSize;
                            focal = std::max ( width, height ) * exifReader->getFocal() / ccdw;
                            os << *iter_image << ";" << width << ";" << height << ";" << focal << ";" << sCamName << ";" << sCamModel << std::endl;

                            _callback->onCreateListCameraModel(sImageFilename,
                                                               true,
                                                               focal,
                                                               width,
                                                               height,
                                                               sCamName,
                                                               sCamModel);
                        }
                        else
                        {
                            std::cout << "Camera \"" << sCamName << "\" model \"" << sCamModel << "\" doesn't exist in the database" << std::endl;
                            os << *iter_image << ";" << width << ";" << height << ";" << sCamName << ";" << sCamModel << std::endl;

                            _callback->onCreateListCameraModel(sImageFilename,
                                                               false,
                                                               _sFocalLength,
                                                               width,
                                                               height,
                                                               sCamName,
                                                               sCamModel);
                        }
                    }
                    else
                    {
                        // Camera model was not recognized, or _sSensorWidthDatabase was not a valid file
                        os << *iter_image << ";" << width << ";" << height << ";" << sCamName << ";" << sCamModel << std::endl;

                        _callback->onCreateListCameraModel(sImageFilename,
                                                           false,
                                                           _sFocalLength,
                                                           width,
                                                           height,
                                                           sCamName,
                                                           sCamModel);
                    }
                }
                listTXT << os.str();
            }
        }
        else
        {

            _callback->onError(omvgPmvsCallback::LISTSTXT_NOT_OPENED);
            bOk = false;
        }
    }
    return bOk;
}

/**
 * Computes the matches between each pair of input pictures.
 * @todo Instead of re-reading data from _sInputDirectory and
 *   _sOutputDirectory, reuse the variables from createList.
 * @return Whether the algorithm has passed (true) or failed (false).
 */
bool omvgPmvsReconstructionEngine::computeMatches()
{
    /////////////////
    // List images //
    /////////////////

    std::string sListsFile = stlplus::create_filespec(_sMatchesDirectory,
                                                      "lists.txt").c_str();
    std::vector<openMVG::SfMIO::CameraInfo> vec_camImageName;
    std::vector<openMVG::SfMIO::IntrinsicCameraInfo> vec_focalGroup;

    // If we can't load the list of focals, we failed
    if (!openMVG::SfMIO::loadImageList( vec_camImageName,
                                        vec_focalGroup,
                                        sListsFile) )
    {

        _callback->onError(omvgPmvsCallback::LISTSTXT_NOT_OPENED);
        // TODO no early return if possible
        return false;
    }

    std::vector<std::string> vec_fileNames;
    std::vector<std::pair<size_t, size_t> > vec_imagesSize;
    for ( std::vector<openMVG::SfMIO::CameraInfo>::const_iterator iter_camInfo = vec_camImageName.begin();
          iter_camInfo != vec_camImageName.end();
          iter_camInfo++ )
    {
        vec_imagesSize.push_back( std::make_pair( vec_focalGroup[iter_camInfo->m_intrinsicId].m_w,
                                  vec_focalGroup[iter_camInfo->m_intrinsicId].m_h ) );
        vec_fileNames.push_back( stlplus::create_filespec( _sInputDirectory, iter_camInfo->m_sImageName) );
    }

    ///////////////////////////////////////////////
    // Compute features and descriptors          //
    //   Extract sift features and descriptors   //
    //   If we already have keypoints, load them //
    //   Else save everything (useful for debug) //
    ///////////////////////////////////////////////


    _callback->onStatusChange(omvgPmvsCallback::COMPUTE_FEATURES);

    typedef Descriptor<float, 128> DescriptorT;
    typedef SIOPointFeature FeatureT;
    typedef std::vector<FeatureT> FeatsT;
    typedef vector<DescriptorT > DescsT;
    typedef KeypointSet<FeatsT, DescsT > KeypointSetT;

    {
        // Extract features
        vec_imagesSize.resize(vec_fileNames.size());

        Image<RGBColor> imageRGB;
        Image<unsigned char> imageGray;

        for(size_t i=0; i < vec_fileNames.size(); ++i)
        {
            KeypointSetT kpSet;

            std::string sFeat = stlplus::create_filespec(_sMatchesDirectory,
                                                         stlplus::basename_part(vec_fileNames[i]), "feat");
            std::string sDesc = stlplus::create_filespec(_sMatchesDirectory,
                                                         stlplus::basename_part(vec_fileNames[i]), "desc");

            // Test if descriptor and feature were already computed
            if (stlplus::file_exists(sFeat) && stlplus::file_exists(sDesc))
            {
                if (ReadImage(vec_fileNames[i].c_str(), &imageRGB))
                {
                    vec_imagesSize[i] = make_pair(imageRGB.Width(), imageRGB.Height());
                }
                else
                {
                    ReadImage(vec_fileNames[i].c_str(), &imageGray);
                    vec_imagesSize[i] = make_pair(imageGray.Width(), imageGray.Height());
                }
            }
            else // Not already computed, compute and save
            {
                if (ReadImage(vec_fileNames[i].c_str(), &imageRGB))
                {
                    Rgb2Gray(imageRGB, &imageGray);
                }
                else
                {
                    ReadImage(vec_fileNames[i].c_str(), &imageGray);
                }

                // Compute features and descriptors and export them to file
                SIFTDetector(imageGray,
                             kpSet.features(), kpSet.descriptors(),
                             _bOctMinus1, true, _dPeakThreshold);

                kpSet.saveToBinFile(sFeat, sDesc);
                vec_imagesSize[i] = make_pair(imageGray.Width(), imageRGB.Height());
            }
            // Update callback

            _callback->onComputeMatchesFeaturesExtracted(vec_fileNames[i],
                                                         i+1,
                                                         vec_fileNames.size());
        }
    }

    //////////////////////////////////////////////
    // Compute putative descriptor matches      //
    //   L2 Descriptor matching                 //
    //   Keep iif NearestNeighbor ratio is kept //
    //////////////////////////////////////////////


    _callback->onStatusChange(omvgPmvsCallback::MATCHING_PAIRS);

    IndexedMatchPerPair map_PutativesMatches;
    // Define the matcher and the used metric (Squared L2)
    // ANN matcher could be defined as follow:
    typedef flann::L2<DescriptorT::bin_type> MetricT;
    typedef ArrayMatcher_Kdtree_Flann<DescriptorT::bin_type, MetricT> MatcherT;
    // Brute force matcher is defined as following:
    //typedef L2_Vectorized<DescriptorT::bin_type> MetricT;
    //typedef ArrayMatcherBruteForce<DescriptorT::bin_type, MetricT> MatcherT;

    // If the matches already exists, reload them
    if (stlplus::file_exists(_sMatchesDirectory + "/matches.putative.txt"))
    {
        PairedIndMatchImport(_sMatchesDirectory + "/matches.putative.txt", map_PutativesMatches);
        std::cout << std::endl << "PUTATIVE MATCHES -- PREVIOUS RESULTS LOADED" << std::endl;
    }
    else // Compute the putatives matches
    {
        ImageCollectionMatcher_AllInMemory<KeypointSetT, MatcherT> collectionMatcher(_fDistRatio);
        if (collectionMatcher.loadData(vec_fileNames, _sMatchesDirectory))
        {
            std::cout  << std::endl << "PUTATIVE MATCHES" << std::endl;
            collectionMatcher.Match(vec_fileNames, map_PutativesMatches);
            //---------------------------------------
            //-- Export putative matches
            //---------------------------------------
            std::ofstream file (std::string(_sMatchesDirectory + "/matches.putative.txt").c_str());
            if (file.is_open())
                PairedIndMatchToStream(map_PutativesMatches, file);
            file.close();
        }
    }
    //-- export putative matches Adjacency matrix
    PairWiseMatchingToAdjacencyMatrixSVG(vec_fileNames.size(),
                                         map_PutativesMatches,
                                         stlplus::create_filespec(_sMatchesDirectory, "PutativeAdjacencyMatrix", "svg"));

    ///////////////////////////////////////////////////////
    // Perform geometric filtering on putative matchees  //
    //   AContrario estimation of fundamental matrix     //
    //   Use upper bound for the most plausible F matrix //
    ///////////////////////////////////////////////////////


    _callback->onStatusChange(omvgPmvsCallback::FILTERING_MATCHES);

    IndexedMatchPerPair map_GeometricMatches_F;

    GeometricFilter_FMatrix_AC geomFilter_F_AC(4.0);
    ImageCollectionGeometricFilter<FeatureT, GeometricFilter_FMatrix_AC> collectionGeomFilter;
    if (collectionGeomFilter.loadData(vec_fileNames, _sMatchesDirectory))
    {
        std::cout << std::endl << " - GEOMETRIC FILTERING - " << std::endl;
        collectionGeomFilter.Filter(
                    geomFilter_F_AC,
                    map_PutativesMatches,
                    map_GeometricMatches_F,
                    vec_imagesSize);

        //---------------------------------------
        //-- Export geometric filtered matches
        //---------------------------------------
        std::ofstream file (string(_sMatchesDirectory + "/matches.f.txt").c_str());
        if (file.is_open())
            PairedIndMatchToStream(map_GeometricMatches_F, file);
        file.close();

        //-- export Adjacency matrix
        std::cout << "\n Export Adjacency Matrix of the pairwise's Epipolar matches"
                  << std::endl;
        PairWiseMatchingToAdjacencyMatrixSVG(vec_fileNames.size(),
                                             map_GeometricMatches_F,
                                             stlplus::create_filespec(_sMatchesDirectory, "EpipolarAdjacencyMatrix", "svg"));
    }

    return true;
}

/**
 * Applies incremental SfM to the set of pictures, and converts it to a
 * usable PMVS project. At this point it is assumed _sOutputDirectory isn't
 * empty.
 * @todo Offer the possibility to choose initial pair to the user
 * @return Whether the algorithm has passed (true) or failed (false).
 */
bool omvgPmvsReconstructionEngine::incrementalSfm()
{

    _callback->onStatusChange(omvgPmvsCallback::SFM);

    std::pair<size_t,size_t> initialPair(0,0);
    bool bPmvsExport = true; // Whether to export to PMVS. Should be left to true.
    bool bOk = true;

    clock_t timeStart = clock();
    IncrementalReconstructionEngine to3DEngine(_sInputDirectory,
                                               _sMatchesDirectory,
                                               _sOutputDirectory,
                                               true);

    to3DEngine.setInitialPair(initialPair);
    to3DEngine.setIfRefinePrincipalPointAndRadialDisto(_bRefinePPandDisto);

    if (to3DEngine.Process())
    {
        clock_t timeEnd = clock();

        // Compute and export colorized point cloud
        std::vector<Vec3> colortracks;
        to3DEngine.ColorizeTracks(colortracks);
        const reconstructorHelper & reconstructorHelperRef = to3DEngine.refToReconstructorHelper();
        reconstructorHelperRef.exportToPly(
                    stlplus::create_filespec(_sOutputDirectory, "FinalColorized", ".ply"),
                    &colortracks);

        // Export to PMVS project directory format for dense reconstruction
        // The project will be in _sOutputDirectory/PMVS
        if (bPmvsExport)  {
            std::cout << std::endl << "Export 3D scene to PMVS format" << std::endl;
            reconstructorHelperRef.exportToPMVSFormat(
                        stlplus::folder_append_separator(_sOutputDirectory) + "PMVS",
                        to3DEngine.getFilenamesVector(),
                        _sInputDirectory);
        }
    }
    else
    {

        _callback->onError(omvgPmvsCallback::FAILED_SFM);
        bOk = false;
    }
    return bOk;
}

/**
 * Uses PMVS to compute a dense reconstruction of the scene, from the
 * PMVS project located at _sOutputDirectory/PMVS.
 * @todo Use the pmvs library instead of an executable, if possible.
 * @return Whether the algorithm has passed (true) or failed (false).
 */
bool omvgPmvsReconstructionEngine::denseReconstruction() {

    _callback->onStatusChange(omvgPmvsCallback::DENSE_RECONSTRUCTION);

    bool bOk = true;

    // Warning : this is highly nauseating. Use at your own risk. To be
    // fixed.
    std::string command = _sPathToPmvs;
    // Be wary here, pmvs awaits a / at the end of its first argument
    // or you'll be greeted with a wonderful "Unrecognizable argument"
    command.append(" " + stlplus::folder_append_separator(
                       stlplus::folder_append_separator(_sOutputDirectory)
                       + "PMVS")
                   +" pmvs_options.txt");
    std::cout << "Calling : " << command << std::endl;
    int ret = system(command.c_str());

    if(ret != 0)
    {

        _callback->onError(omvgPmvsCallback::FAILED_DENSE_RECONSTRUCTION);
        bOk = false;
    }

    if(bOk)
    {
        _callback->onStatusChange(omvgPmvsCallback::DENSE_RECONSTRUCTION_FINISHED);
    }

    return(bOk);
}

/**
 * @brief omvgPmvsReconstructionEngine::setCallback sets a custom callback
 *  for knowing the state of the reconstruction (see omvgPmvsCallback).
 *  Be wary not to delete the callback, we are just pointing on your instance
 *  here.
 * @param callback Callback to be used.
 */
void omvgPmvsReconstructionEngine::setCallback(omvgPmvsCallback& callback)
{
    delete(this->_callback);
    this->_callback = &callback;
}

/**
 * @brief omvgPmvsReconstructionEngine::setNearestNeighborDistanceRatio
 * @param fDistRatio The new nearest neighbor distance ratio (default = 0.6f)
 *  for putative descriptor matches
 */
void omvgPmvsReconstructionEngine::setNearestNeighborDistanceRatio(float fDistRatio)
{
    this->_fDistRatio = fDistRatio;
}

/**
 * @brief omvgPmvsReconstructionEngine::getNearestNeighborDistanceRatio
 * @return The current nearest neighbor distance ratio (default = 0.6f) for
 *  putative descriptor matches
 */
float omvgPmvsReconstructionEngine::getNearestNeighborDistanceRatio()
{
    return this->_fDistRatio;
}

/**
 * @brief omvgPmvsReconstructionEngine::setInputDirectory
 * @param sInputDirectory The new input directory
 */
void omvgPmvsReconstructionEngine::setInputDirectory(string sInputDirectory)
{
    this->_sInputDirectory = sInputDirectory;
}

/**
 * @brief omvgPmvsReconstructionEngine::getInputDirectory
 * @return The current input directory
 */
string omvgPmvsReconstructionEngine::getInputDirectory()
{
    return this->_sInputDirectory;
}

/**
 * @brief omvgPmvsReconstructionEngine::setOutputDirectory
 * @param sOutputDirectory The new output directory
 */
void omvgPmvsReconstructionEngine::setOutputDirectory(string sOutputDirectory)
{
    this->_sOutputDirectory = sOutputDirectory;
    this->_sMatchesDirectory = stlplus::folder_append_separator(_sOutputDirectory) + "matches";
}

/**
 * @brief omvgPmvsReconstructionEngine::getOutputDirectory
 * @return The current output directory
 */
string omvgPmvsReconstructionEngine::getOutputDirectory()
{
    return this->_sOutputDirectory;
}

/**
 * @brief omvgPmvsReconstructionEngine::setPathToPmvs
 * @param sPathToPmvs The new path to the PMVS executable
 */
void omvgPmvsReconstructionEngine::setPathToPmvs(string sPathToPmvs)
{
    this->_sPathToPmvs = sPathToPmvs;
}

/**
 * @brief omvgPmvsReconstructionEngine::getPathToPmvs
 * @return The current path to the PMVS executable
 */
string omvgPmvsReconstructionEngine::getPathToPmvs()
{
    return this->_sPathToPmvs;
}

/**
 * @brief omvgPmvsReconstructionEngine::setDefaultFocalLength
 * @param sFocalLength The new default focal length
 */
void omvgPmvsReconstructionEngine::setDefaultFocalLength(int sFocalLength)
{
    this->_sFocalLength = sFocalLength;
}

/**
 * @brief omvgPmvsReconstructionEngine::getDefaultFocalLength
 * @return The current default focal length
 */
int omvgPmvsReconstructionEngine::getDefaultFocalLength()
{
    return this->_sFocalLength;
}

/**
 * @brief omvgPmvsReconstructionEngine::setSensorWidthDatabasePath
 * @param sSensorWidthDatabasePath The path to the new sensor width database
 */
void omvgPmvsReconstructionEngine::setSensorWidthDatabasePath(string sSensorWidthDatabasePath)
{
    this->_sSensorWidthDatabase = sSensorWidthDatabasePath;
}

/**
 * @brief omvgPmvsReconstructionEngine::getSensorWidthDatabasePath
 * @return The current path to the sensor width database
 */
string omvgPmvsReconstructionEngine::getSensorWidthDatabasePath()
{
    return this->_sSensorWidthDatabase;
}

/**
 * @brief omvgPmvsReconstructionEngine::getSiftFirstOctaveMinusOne
 * @return Whether we will use octave -1 option of SIFT (default = false)
 */
bool omvgPmvsReconstructionEngine::getSiftFirstOctaveMinusOne() const
{
    return _bOctMinus1;
}

/**
 * @brief omvgPmvsReconstructionEngine::setSiftFirstOctaveMinusOne
 * @param bOctMinus1 Whether to use the octave -1 option of SIFT (default = false)
 */
void omvgPmvsReconstructionEngine::setSiftFirstOctaveMinusOne(bool bOctMinus1)
{
    _bOctMinus1 = bOctMinus1;
}

/**
 * @brief omvgPmvsReconstructionEngine::getPeakThreshold
 * @return The peak threshold option of SIFT (default = 0.04f)
 */
float omvgPmvsReconstructionEngine::getPeakThreshold() const
{
    return _dPeakThreshold;
}

/**
 * @brief omvgPmvsReconstructionEngine::setPeakThreshold
 * @param dPeakThreshold The new peak threshold option of SIFT (default = 0.04f)
 */
void omvgPmvsReconstructionEngine::setPeakThreshold(float dPeakThreshold)
{
    _dPeakThreshold = dPeakThreshold;
}

/**
 * @brief omvgPmvsReconstructionEngine::getRefinePPandDisto
 * @return Whether to refine principal point and radial distorsion (default = true)
 */
bool omvgPmvsReconstructionEngine::getRefinePPandDisto() const
{
    return _bRefinePPandDisto;
}

/**
 * @brief omvgPmvsReconstructionEngine::setRefinePPandDisto
 * @param bRefinePPandDisto Whether to refine principal point and radial distorsion
 *  (default = true)
 */
void omvgPmvsReconstructionEngine::setRefinePPandDisto(bool bRefinePPandDisto)
{
    _bRefinePPandDisto = bRefinePPandDisto;
}



