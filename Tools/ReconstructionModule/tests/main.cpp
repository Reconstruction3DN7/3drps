#include "../omvgPmvsReconstructionEngine.h"
#include <iostream>

using namespace std;

class sampleCallback : public omvgPmvsCallback {
    // omvgPmvsCallback interface
public:
    void onStatusChange(ReconstructionStatus status)
    {
        switch(status) {
        case omvgPmvsCallback::CREATE_LISTS:
            cout << "Creating lists of picture parameters" << endl;
            break;

        case omvgPmvsCallback::COMPUTE_FEATURES:
            cout << "Computing features" << endl;
            break;

        case omvgPmvsCallback::MATCHING_PAIRS:
            cout << "Matching pairs" << endl;
            break;

        case omvgPmvsCallback::FILTERING_MATCHES:
            cout << "Filtering matches" << endl;
            break;

        case omvgPmvsCallback::SFM:
            cout << "Doing SfM" << endl;
            break;

        case omvgPmvsCallback::DENSE_RECONSTRUCTION:
            cout << "Doing dense reconstruction" << endl;
            break;

        default:
            cerr << "onstatuschange called : " << status;
        }
    }

    void onError(ReconstructionError error) {
        cout << "Error detected - this is abnormal." << cout;
    }

    void onCreateListCameraModel(string imageFileName, bool cameraRecognized, double focal, size_t width, size_t height, string camName, string camModel)
    {
        cout << "CLCM : "
             << imageFileName
             << (cameraRecognized ? " RECOGNIZED" : "UNKNOWN")
             << " focal : " << focal
             << " width : " << width
             << " height : " << height
             << " Camera : " << camName << " " << camModel
             << endl;
    }

    void onComputeMatchesFeaturesExtracted(string imageFileName, int imageNumber, int totalImagesNumber) {
        cout << "ComputeMatches :"
             << imageFileName
             << " image number " << imageNumber
             << " of " << totalImagesNumber
             << endl;
    }
};

int main()
{
    omvgPmvsReconstructionEngine p("./in","./out","./CameraGenerated.txt");
    sampleCallback c;
    p.setCallback(c);
    p.createList();
    p.computeMatches();
    p.incrementalSfm();
    p.denseReconstruction();
	return EXIT_SUCCESS;
}
