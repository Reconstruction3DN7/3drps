#ifndef SIMPLEPROGRESS_H
#define SIMPLEPROGRESS_H

#include "MeshImprimator.h"
#include <iostream>
#include <cstdlib>

class simpleProgress : public IProgress
{
public:
    // Constructor
    simpleProgress();
    ~simpleProgress();
    //
    void setValue(int value);
    bool wasCanceled() const;
    void setRange(int min, int max);

private:
    int _min, _max, _value;
};

#endif // SIMPLEPROGRESS_H
