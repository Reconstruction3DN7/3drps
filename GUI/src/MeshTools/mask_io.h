#ifndef MASK_IO_H
#define MASK_IO_H

#include <string>

#include <MeshImprimator.h>

void WriteMask(const MaskData::Mask& mask, const std::string& filename);
void ReadMask(MaskData::Mask& mask, const std::string& filename);

#endif
