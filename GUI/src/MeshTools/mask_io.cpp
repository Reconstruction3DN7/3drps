#include <climits>

#include <iostream>
#include <fstream>

#include "mask_io.h"

using namespace std;

void WriteMask(const MaskData::Mask& mask, const std::string& filename)
{
    ofstream file(filename.c_str());

    if (!file)
    {
        cerr << "Cannot open file <" << filename << "> for writing." << endl;
        return;
    }

    file << mask.rows() << "\n";
    file << mask.cols() << "\n";

    for (int i = 0; i < mask.rows(); i++)
    {
        for (int j = 0; j < mask.cols(); j++)
        {
            file << (mask(i, j) > 0 ? '1' : '0');
        }

        file << "\n";
    }
}

void ReadMask(MaskData::Mask& mask, const std::string& filename)
{
    ifstream maskFile;
    int nbRows, nbCols;

    maskFile.open(filename.c_str());
    if (!maskFile)
    {
        cerr << "Cannot open " << filename << endl;
        return;
    }

    maskFile >> nbRows >> nbCols;
    maskFile.ignore();

    mask.resize(nbRows, nbCols);
    mask.setZero();

    for(int i = 0; i < nbRows; i++)
    {
        string line;
        getline(maskFile, line);

        if ((int) line.size() < nbCols)
        {
            cerr << "Error in mask at line " << i << endl;
            return;
        }

        for (int j = 0; j < nbCols; j++)
        {
            switch (line[j])
            {
            case '0':
                break;
            case '1':
                mask(i, j) = SCHAR_MAX;
                break;
            default:
                cerr << "Unknown character: " << int(line[j]) << endl;
            }
        }
    }

}


