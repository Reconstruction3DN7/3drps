#include "meshReconstructionEngine.h"
#include <QtCore>

meshReconstructionEngine::meshReconstructionEngine(QWidget* window,
                                                   std::string inPutDirectoryCamera,
                                                   std::string inPutDirectoryMask,
                                                   std::string inPutFilePly,
                                                   std::string inPutFileStl,
                                                   int epsilon,
                                                   DelaunayReconstructor::MethodChoice methodChoice)
{
    _window = window;
    _inPutDirectoryCamera = inPutDirectoryCamera;
    _inPutDirectoryMask = inPutDirectoryMask;
    _inPutFilePly = inPutFilePly;
    _outPutFileStl = inPutFileStl;
    _epsilon = epsilon;
    _methodChoice = methodChoice;

    _callback = new meshReconstructionCallback();
}

/**
 * Sample destructor for meshReconstructionEngine.
 */
meshReconstructionEngine::~meshReconstructionEngine()
{
}

void meshReconstructionEngine::setInPutDirectoryMask(std::string inPutDirectoryMask)
{
    this->_inPutDirectoryMask = inPutDirectoryMask;
}

void meshReconstructionEngine::setInPutDirectoryCamera(std::string inPutDirectoryCamera)
{
    this->_inPutDirectoryCamera = inPutDirectoryCamera;
}

void meshReconstructionEngine::setInPutFilePly(std::string inPutFilePly)
{
    this->_inPutFilePly = inPutFilePly;
}

void meshReconstructionEngine::setOutPutFileStl(std::string outPutFileSTL)
{
    this->_outPutFileStl = outPutFileSTL;
}

void meshReconstructionEngine::setCallback(meshReconstructionCallback& callback)
{
    delete(this->_callback);
    this->_callback = &callback;
}

bool meshReconstructionEngine::Process()
{    
    _callback->onStatusChange(meshReconstructionCallback::MASK_CAMERA_READING);

    bool mre = true;

    // Create a maskdatalist
    std::list<MaskData> maskDataList;

    // For each mask file
    QDir maskDirectory(QString::fromStdString(_inPutDirectoryMask));
    QFileInfoList masks = maskDirectory.entryInfoList(
                    QStringList("*.mask"),
                    QDir::Files | QDir::Readable);
    foreach (QFileInfo mask, masks)
    {
        // Create a maskData
        MaskData maskData;

        // Read the maskfile and put it in maskData.mask
        QString maskPath = mask.absoluteFilePath();
        ReadMask(maskData.mask, maskPath.toStdString());
        cerr << "Found " << maskPath.toStdString() << endl;

        // Read the corresponding camera file, put it in maskData.camera
        maskData.camera.setZero();

        QFile cameraFile(QString::fromStdString(_inPutDirectoryCamera) + mask.baseName() + ".txt");
        if (!cameraFile.open(QIODevice::ReadOnly | QIODevice::Text))
            continue;

        cerr << "Read camera file : " << _inPutDirectoryCamera << mask.baseName().toStdString() << ".txt" << endl;

        QTextStream stream(&cameraFile);
        stream.readLine();                  // camera files begin with "CONTOUR\n" so deal with it

        for (int i = 0; i < 3; i++)
                    for (int j = 0; j < 4; j++)
                        stream >> maskData.camera(i, j);
        cout << "Camera: " << endl << maskData.camera << endl;

        // Add maskData to maskDataList
        maskDataList.push_back(maskData);
    }

    cout << "Found " << maskDataList.size() << " masks and cams." << endl;

    // Ply reading
     _callback->onStatusChange(meshReconstructionCallback::PLY_READING);
    PLYReader reader;
    // Open the ply file
    cout << "Reading PLY file : " << _inPutFilePly << endl; cout.flush();
    reader.open(_inPutFilePly);

    int errorCode, line;

    if (!reader.readHeader(&errorCode, &line))
    {
        std::cerr << "Error: " << errorCode << std::endl;
        std::cerr << "Line: " << line << std::endl;

        return EXIT_FAILURE;
    }

    std::cerr << "Header successfully read." << std::endl;

    if (!reader.readContent(&errorCode, &line))
    {
        std::cerr << "Error: " << errorCode << std::endl;
        std::cerr << "Line: " << line << std::endl;

        return EXIT_FAILURE;
    }

    std::cerr << "Content successfully read." << std::endl;

    reader.close();

    cout.flush();
    // Create the point cloud
    const Ply::Object* obj = reader.getObject();
    PointCloud cloud = obj->getPointCloud();

    // Create and call a DelaunayReconstructor
     _callback->onStatusChange(meshReconstructionCallback::DELAUNAY_RECONSTRUCT);
    IProgress* progress = new simpleProgress();
    IMeshReconstructor* reconstructionMesh = new DelaunayReconstructor(cloud, maskDataList, _epsilon, _methodChoice);
    // Create the mesh
    TriangleMesh triangleMesh = reconstructionMesh->reconstruct(progress);
    // Create te final stl file
     _callback->onStatusChange(meshReconstructionCallback::STL_CREATION);
    STLWriter writer;
    writer.write(triangleMesh, _outPutFileStl, _outPutFileStl);
    cout << endl <<  "Mesh reconstruction complete" << endl;
    _callback->onStatusChange(meshReconstructionCallback::MESH_RECONSTRUCTION_FINISHED);

     return mre;
}
