#ifndef MESHRECONSTRUCTIONENGINE_H
#define MESHRECONSTRUCTIONENGINE_H

#include "MeshImprimator.h"
#include "mask_io.h"
#include "xml_io.h"
#include "tinyxml2.h"
#include "simpleProgress.h"
#include <iostream>
#include <cstdlib>
#include <QString>
#include <QFileDialog>
#include <QInputDialog>
#include <QWidget>

#include "src/CallbackTools/meshReconstructionCallback.h"


using namespace tinyxml2;
using namespace std;

class meshReconstructionEngine
{
public:
    meshReconstructionEngine(QWidget* window,
                             std::string inPutDirectoryCamera,
                             std::string inPutDirectoryMaskMask,
                             std::string inPutFilePly,
                             std::string outPutFileStl,
                             int epsilon,
                             DelaunayReconstructor::MethodChoice methodChoice);

    ~meshReconstructionEngine();

    bool Process();

    // Set up a callback
    void setCallback(meshReconstructionCallback& callback);

    void setInPutDirectoryCamera(std::string inPutDirectoryCamera);
    void setInPutDirectoryMask(std::string inPutDirectoryMask);
    void setInPutFilePly(std::string inPutFilePly);
    void setOutPutFileStl(std::string outPutFileStl);

private:
    QWidget* _window;
    std::string _inPutDirectoryCamera;
    std::string _inPutDirectoryMask;
    std::string _inPutFilePly;
    std::string _outPutFileStl;
    int _epsilon;
    DelaunayReconstructor::MethodChoice _methodChoice;

    meshReconstructionCallback * _callback;       // Callback to handle states and errors

};

#endif // MESHRECONSTRUCTIONENGINE_H
