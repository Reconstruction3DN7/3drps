#include "simpleProgress.h"

simpleProgress::simpleProgress()
{
    _min = 0,
    _max = 0,
    _value = 0;
}

simpleProgress::~simpleProgress()
{
}

void simpleProgress::setValue(int value)
{
    _value = value;

    int percent = 100 * static_cast<float>(value - _min) / (_max - _min);
    std::cerr << "\r" << percent << " %";
}

void simpleProgress::setRange(int min, int max)
{
    _min = min;
    _max = max;
}

bool simpleProgress::wasCanceled() const
{
    return false;
}


