#ifndef XML_IO_H
#define XML_IO_H

#include <QWidget>

bool writeXML(const QString& cameraDirectoryPath,
              const QString& maskDirectoryPath,
              const QString& outputFilename,
              const QString& relativePath);

#endif // XML_IO_H
