#include "xml_io.h"

#include <MeshImprimator.h>

#include <QtCore>
#include <QFileDialog>

bool writeXML(const QString &cameraDirectoryPath,
              const QString &maskDirectoryPath,
              const QString &outputFilename,
              const QString &relativePath)
{

    QDir cameraDirectory(cameraDirectoryPath);
    QDir maskDirectory(maskDirectoryPath);

    if (!cameraDirectory.exists() && !maskDirectory.exists())
        return false;

    QFile file(outputFilename);
    if (!file.open(QIODevice::ReadWrite | QIODevice::Text))
        return false;

    QXmlStreamWriter xml(&file);
    xml.setAutoFormatting(true);
    xml.writeStartDocument();

    xml.writeStartElement("cameras");

    QFileInfoList masks = maskDirectory.entryInfoList(
                QStringList("*.mask"),
                QDir::Files | QDir::Readable);

    foreach (QFileInfo mask, masks)
    {
        QString name = mask.baseName();

        QFile cameraFile(cameraDirectory.filePath(name + ".txt"));
        if (!cameraFile.open(QIODevice::ReadOnly | QIODevice::Text))
            continue;

        QString maskPath = QString("%1/%2")
                .arg(relativePath)
                .arg(mask.fileName());

        MaskData::CameraMatrix camera;
        camera.setZero();

        QTextStream stream(&cameraFile);
        stream.readLine();

        for (int i = 0; i < 3; i++)
            for (int j = 0; j < 4; j++)
                stream >> camera(i, j);

        xml.writeStartElement("camera");

        xml.writeStartElement("camera-matrix");
        for (int i = 0; i < 3; i++)
        {
            xml.writeStartElement("row");
            for (int j = 0; j < 4; j++)
            {
                xml.writeTextElement("cell", QString::number(camera(i, j)));
            }
            xml.writeEndElement();
        }
        xml.writeEndElement();

        xml.writeStartElement("camera-path");
        xml.writeTextElement("image-path", maskPath);
        xml.writeEndElement();


        xml.writeEndElement();
    }

    xml.writeEndElement();
    return true;
}
