#ifndef MESHRECONSTRUCTIONCALLBACK_H
#define MESHRECONSTRUCTIONCALLBACK_H

#include "ui_mainwindow.h"
#include <iostream>
#include <QObject>

class meshReconstructionCallback : public QObject
{
    Q_OBJECT
public:
    /**
     * @brief This is an enumeration of all status flags possible when using
     *  MeshReconstructionEngine
     */
    enum MeshReconstructionStatus
    {
        /// Not doing anything
        IDLE,
        /// Read the masks and camera files
        MASK_CAMERA_READING,
        /// Read the ply file
        PLY_READING,
        /// Delaunay reconstruction
        DELAUNAY_RECONSTRUCT,
        /// Creation of the stl final file
        STL_CREATION,
        /// 3D Reconstruction finished
        MESH_RECONSTRUCTION_FINISHED
    };

    /**
     * @brief This is an enumeration of all error flags possible when using
     *  MeshReconstructionEngine
     */
    enum MeshReconstructionError
    {
        /// Everything's fine
        NONE,
    };

    meshReconstructionCallback() {}

    void onStatusChange(MeshReconstructionStatus status);
    void onError(MeshReconstructionError error);

signals:
    void statusChanged(std::string statusMessage);
    void errorRaised(std::string errorMessage);
    void reconstructionFinished();
};
#endif // MESHRECONSTRUCTIONCALLBACK_H
