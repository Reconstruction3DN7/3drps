#ifndef POINTRECONSTRUCTION_H
#define POINTRECONSTRUCTION_H

#include "omvgPmvsCallback.h"
#include "ui_mainwindow.h"
#include <iostream>
#include <QObject>

class pointReconstructionCallback : public QObject,
                    public omvgPmvsCallback
{
    Q_OBJECT
public:
    pointReconstructionCallback() {}
    void onStatusChange(ReconstructionStatus status);
    void onError(ReconstructionError error);
    void onCreateListCameraModel(std::string imageFileName,
                                             bool cameraRecognized,
                                             double focal = -1,
                                             size_t width = -1,
                                             size_t height = -1,
                                             std::string camName = "",
                                             std::string camModel = "");
    void onComputeMatchesFeaturesExtracted(std::string imageFileName, int imageNumber, int totalImagesNumber);
signals:
    void statusChanged(std::string statusMessage);
    void errorRaised(std::string errorMessage);
    void reconstructionFinished();
};

#endif // POINTRECONSTRUCTIONCALLBACK_H
