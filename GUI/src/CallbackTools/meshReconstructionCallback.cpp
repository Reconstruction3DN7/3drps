#include "meshReconstructionCallback.h"

using namespace std;

void meshReconstructionCallback::onStatusChange(MeshReconstructionStatus status)
{
    string s;
    switch(status)
    {
    case(meshReconstructionCallback::MASK_CAMERA_READING):
        s = "-- Reading the masks and camera files";
        break;
    case(meshReconstructionCallback::PLY_READING):
        s = "-- Reading the ply file (point cloud)";
        break;
    case(meshReconstructionCallback::DELAUNAY_RECONSTRUCT):
        s = "-- Performing Delaunay Reconstruction";
        break;
    case(meshReconstructionCallback::STL_CREATION):
        s = "-- Creation of the stl file";
        break;
    case(meshReconstructionCallback::MESH_RECONSTRUCTION_FINISHED):
        s = "-- 3D Reconstruction finished";
        emit reconstructionFinished();
        break;
    emit statusChanged(s);
    }
}
