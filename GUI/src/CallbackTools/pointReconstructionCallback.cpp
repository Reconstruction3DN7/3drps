#include "pointReconstructionCallback.h"

using namespace std;

void pointReconstructionCallback::onComputeMatchesFeaturesExtracted(std::string imageFileName, int imageNumber, int totalImagesNumber)
{

}

void pointReconstructionCallback::onStatusChange(ReconstructionStatus status)
{
    string s;
    switch(status)
    {
    case(pointReconstructionCallback::CREATE_LISTS):
        s = "-- Creating lists of camera focals";
        break;
    case(pointReconstructionCallback::COMPUTE_FEATURES):
        s = "-- Computing image features for the picture set";
        break;
    case(pointReconstructionCallback::MATCHING_PAIRS):
        s = "-- Matching pairs of pictures";
        break;
    case(pointReconstructionCallback::FILTERING_MATCHES):
        s = "-- Filtering matches";
        break;
    case(pointReconstructionCallback::SFM):
        s = "-- Performing sparse reconstruction via sfm";
        break;
    case(pointReconstructionCallback::DENSE_RECONSTRUCTION):
        s = "-- Performing dense reconstruction";
        break;
    case(pointReconstructionCallback::DENSE_RECONSTRUCTION_FINISHED):
        s = "-- Dense reconstruction finished";
        emit reconstructionFinished();
        break;
    }
    emit statusChanged(s);
}

void pointReconstructionCallback::onError(ReconstructionError error)
{
    string e = "Error : ";
    switch(error)
    {
    case(pointReconstructionCallback::EMPTY_IMAGE_LIST):
        e += "Empty image list.";
        break;
    case(pointReconstructionCallback::LISTSTXT_NOT_OPENED):
        e +=  "Lists.txt was not opened.";
        break;
    case(pointReconstructionCallback::FAILED_DENSE_RECONSTRUCTION):
        e +=  "Dense reconstruction was a failure.";
        break;
    case(pointReconstructionCallback::FAILED_SFM):
        e +=  "Sparse reconstruction failed.";
        break;
    default:
        e +=  "Unknown.";
        break;
    }
    emit errorRaised(e);
}

void pointReconstructionCallback::onCreateListCameraModel(std::string imageFileName,
                                         bool cameraRecognized,
                                         double focal,
                                         size_t width,
                                         size_t height,
                                         std::string camName,
                                         std::string camModel)
{

}
