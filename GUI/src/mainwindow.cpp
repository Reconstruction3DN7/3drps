#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    _reconstructionEngine("","",""),
    _meshReconstructionEngine(this,"","","","out.stl",1, DelaunayReconstructor::TRIANGULATE),
    _filterEngine(),
    _maskCreationEngine("","",""),
    _pointReconstructionCallback(),
    _meshReconstructionCallback()
{
    ui->setupUi(this);

    // Default Values
    _reconstructionProcessed = false;
    _filteringProcessed = false;
    //_maskCreationProcessed = false;
    _meshReconstructionProcessed =false;

    //Variable
    QSignalMapper *mapper = new QSignalMapper(this);

    //Input-Ouput Directory selection
    connect(ui->inputDirectoryButton, SIGNAL(clicked()), mapper, SLOT(map()));
    mapper->setMapping(ui->inputDirectoryButton, "in");

    connect(ui->outputDirectoryButton, SIGNAL(clicked()), mapper, SLOT(map()));
    mapper->setMapping(ui->outputDirectoryButton, "out");

    connect(mapper, SIGNAL(mapped(const QString &)), this, SLOT(openDirectory(const QString &)));

    connect(ui->cameraFileButton, SIGNAL(clicked()), this, SLOT(onSensorDatabasePathClicked()));

    // Configuration connects and default values
    ui->focalLengthSpinBox->setValue(_reconstructionEngine.getDefaultFocalLength());
    connect(ui->focalLengthSpinBox, SIGNAL(valueChanged(int)), this, SLOT(onFocalLengthChanged(int)));

    ui->nearestNeighborDRSpinBox->setValue(_reconstructionEngine.getNearestNeighborDistanceRatio());
    connect(ui->nearestNeighborDRSpinBox, SIGNAL(valueChanged(double)), this, SLOT(onNearestNeighborDRChanged(double)));

    ui->octaveMinusOneCheckBox->setChecked(_reconstructionEngine.getSiftFirstOctaveMinusOne());
    connect(ui->octaveMinusOneCheckBox, SIGNAL(clicked(bool)), this, SLOT(onOctaveMinusOneClicked(bool)));

    ui->peakTresholdSpinBox->setValue(_reconstructionEngine.getPeakThreshold());
    connect(ui->peakTresholdSpinBox, SIGNAL(valueChanged(double)), this, SLOT(onPeakTresholdChanged(double)));

    ui->refinePpDistoCheckBox->setChecked(_reconstructionEngine.getRefinePPandDisto());
    connect(ui->refinePpDistoCheckBox, SIGNAL(clicked(bool)), this, SLOT(onRefinePpDistoClicked(bool)));

    ui->pmvsPathLineEdit->setText(QString::fromStdString(_reconstructionEngine.getPathToPmvs()));
    connect(ui->pmvsPathButton, SIGNAL(clicked()), this, SLOT(onPmvsPathButtonClicked()));

    // Registering std::string is mandatory since _pointReconstructionCallback is in another thread
    qRegisterMetaType<std::string>("std::string");
    QObject::connect(&_pointReconstructionCallback,&pointReconstructionCallback::statusChanged,
                     this,&MainWindow::setStatus);
    QObject::connect(&_pointReconstructionCallback,&pointReconstructionCallback::errorRaised,
                     this,&MainWindow::handleError);
    QObject::connect(&_pointReconstructionCallback,&pointReconstructionCallback::reconstructionFinished,
                     this,&MainWindow::onReconstructionFinished);

    // MeshReconstruction Signals
    QObject::connect(&_meshReconstructionCallback,&meshReconstructionCallback::statusChanged,
                     this,&MainWindow::setStatus);
    QObject::connect(&_meshReconstructionCallback,&meshReconstructionCallback::reconstructionFinished,
                     this,&MainWindow::onMeshReconstructionFinished);

    //Processes
    connect(ui->maskButton, SIGNAL(clicked()), this, SLOT(maskCreation()));
    connect(ui->pointButton, SIGNAL(clicked()), this, SLOT(pointReconstruction()));
    connect(ui->filteringButton, SIGNAL(clicked()), this, SLOT(filtering()));
    connect(ui->reconstructionButton, SIGNAL(clicked()), this, SLOT(modelReconstruction()));

    //Callback
    _reconstructionEngine.setCallback(_pointReconstructionCallback);
    _meshReconstructionEngine.setCallback(_meshReconstructionCallback);
}

MainWindow::~MainWindow()
{
    delete ui;
}

///////////////////////////////// Console Viewer ////////////////////////////////
void MainWindow::setStatus(std::string status)
{
    this->ui->statusLabel->setText(QString::fromStdString(status));
    this->ui->consoleOutputTextEdit->appendHtml(QString::fromStdString("<b>" +status+ "</b>"));
}

void MainWindow::handleError(string errorMessage)
{
    this->ui->consoleOutputTextEdit->appendHtml(QString::fromStdString(
                                                    "<p style=\"color:red;\">" +
                                                    errorMessage +
                                                    "</p>"));
}

void MainWindow::onReconstructionFinished()
{
    this->_reconstructionProcessed = true;
    ui->pointButton->setEnabled(true);
    ui->maskButton->setEnabled(true);
}

void MainWindow::onMeshReconstructionFinished()
{
    this->_meshReconstructionProcessed = true;
    ui->maskButton->setEnabled(true);
    ui->pointButton->setEnabled(true);
    ui->filteringButton->setEnabled(true);
    ui->reconstructionButton->setEnabled(true);
}

//////////////////// Selection of the directories and files ////////////////////
void MainWindow::onSensorDatabasePathClicked()
{
    QString file_temp = QFileDialog::getOpenFileName(this);
    if(!file_temp.isEmpty())
    {
        _sensorDB = file_temp;
        ui->cameraFileLineEdit->setText(_sensorDB);
        _reconstructionEngine.setSensorWidthDatabasePath(_sensorDB.toStdString());
    }
}

//SLOT Select the input directory and the output directory
void MainWindow::openDirectory(QString value)
{
    QString directory_temp = QFileDialog::getExistingDirectory(this);

    if (!directory_temp.isEmpty()) {
        if (!value.compare("in")) {
            _inputDirectory = directory_temp;
            ui->inputDirectoryLineEdit->setText(_inputDirectory);
            _reconstructionEngine.setInputDirectory(_inputDirectory.toStdString());
        } else {
            _outputDirectory = directory_temp;
            ui->outputDirectoryLineEdit->setText(_outputDirectory);
            _reconstructionEngine.setOutputDirectory(_outputDirectory.toStdString());

            //Creation of the masks directory
            // TODO Vérifier leur existance ou non
            QDir(_outputDirectory).mkdir("masks");
            QDir(_outputDirectory + "/masks").mkdir("classifier");
        }
    }
}

///////////////////////// Configuration of the values /////////////////////////
void MainWindow::onFocalLengthChanged(int newval)
{
    _reconstructionEngine.setDefaultFocalLength(newval);
}

void MainWindow::onNearestNeighborDRChanged(double newval)
{
    _reconstructionEngine.setNearestNeighborDistanceRatio(newval);
}

void MainWindow::onOctaveMinusOneClicked(bool checked)
{
    _reconstructionEngine.setSiftFirstOctaveMinusOne(checked);
}

void MainWindow::onPeakTresholdChanged(double newval)
{
    _reconstructionEngine.setPeakThreshold(newval);
}

void MainWindow::onRefinePpDistoClicked(bool checked)
{
    _reconstructionEngine.setRefinePPandDisto(checked);
}

void MainWindow::onPmvsPathButtonClicked()
{
    QString pmvs_dir = QFileDialog::getOpenFileName(this);
    if(!pmvs_dir.isEmpty())
    {
        ui->pmvsPathLineEdit->setText(pmvs_dir);
        _reconstructionEngine.setPathToPmvs(pmvs_dir.toStdString());
    }
}

///////////////////////////// Processes Modules /////////////////////////////
void MainWindow::pointReconstruction()
{
    // Check if all the directories are defined
    if (!_inputDirectory.isEmpty() && !_outputDirectory.isEmpty() && !_sensorDB.isEmpty()) {
        // Desactivation of the other processes button
        ui->maskButton->setEnabled(false);
        ui->filteringButton->setEnabled(false);
        ui->reconstructionButton->setEnabled(false);

        // Execution of the process
        connect(&_threadEngine,SIGNAL(started()),&_reconstructionEngine,SLOT(process()));
        _reconstructionEngine.moveToThread(&_threadEngine);
        _pointReconstructionCallback.moveToThread(&_threadEngine);
        _threadEngine.start();
    } else {
        std::cout << "One of the directory fields is empty" << std::endl;
    }
}

void MainWindow::maskCreation()
{
    /// Improvements : no operational code in the GUI
    /// Creation of a class to do the operations on the files
    // Check if the oututDirectory is defined and if the filtering process has been executed
    if (!_outputDirectory.isEmpty()) {
        // Desactivation of the other processes button
        ui->pointButton->setEnabled(false);
        ui->filteringButton->setEnabled(false);
        ui->reconstructionButton->setEnabled(false);

        // Creation of the outPut directories
        QDir *directory = new QDir(_outputDirectory);
        directory->mkdir("masks");
        QDir *directory1 = new QDir(_outputDirectory + "/mask/");
        directory1->mkdir("classifier");

        QString outputMaskPath = _outputDirectory + "/masks/";
        QString outputMaskForClassifierPath = _outputDirectory + "/masks/classifier/";
        QString inputPicturesDirectory = _outputDirectory + "/PMVS/visualize/";
        _maskCreationEngine.setInputPath(inputPicturesDirectory.toStdString());
        _maskCreationEngine.setOutputPath(outputMaskPath.toStdString());
        _maskCreationEngine.setMaskForClassifierPath(outputMaskForClassifierPath.toStdString());

        // Variables
        QString extension;
        vector<string> imagesForClassifierName;
        vector<string> imagesName;

        //First Part of the MaskCreationProcess
        // Selection of the pictures (not all of them)
        QStringList files_temp = QFileDialog::getOpenFileNames(this,"Select some pictures", inputPicturesDirectory);
        if (!files_temp.isEmpty()) {
            for (int i = 0; i < files_temp.length(); i++) {
                QString name = files_temp[i];
                name.remove(name.length()-4,4);
                name.remove(0, inputPicturesDirectory.length());
                imagesForClassifierName.push_back(name.toStdString());
            }
            QString temp = files_temp[0];
            extension = temp.remove(0,temp.length()-4);

            // Execution of the process
            _maskCreationEngine.processUserAssistMaskCreation(imagesForClassifierName, extension.toStdString());
        }

        //Second Part of the MaskCreationProcess
        // Selection of all the pictures
        QStringList files_temp1 = QFileDialog::getOpenFileNames(this,"Select ALL the pictures", inputPicturesDirectory);
        std::cout << "test" << std::endl;
        if (!files_temp1.isEmpty()) {
            for (int i = 0; i < files_temp1.length(); i++) {
                QString name = files_temp1[i];
                name.remove(name.length()-4,4);
                name.remove(0, inputPicturesDirectory.length());
                imagesName.push_back(name.toStdString());
            }
            // Execution of the process
            _maskCreationEngine.processAutomaticMaskCreation(imagesName, imagesForClassifierName, extension.toStdString());
        }

        // Reactivation of the pointButton
        ui->reconstructionButton->setEnabled(true);
        ui->pointButton->setEnabled(true);
        ui->filteringButton->setEnabled(true);

    } else {
        std::cout << "Output Directory Empty" << std::endl;
    }
}

void MainWindow::filtering()
{
    // Check if the oututDirectory is defined and if the Point Reconstruction have been exuted -> Creation of PMVS directory
    if (!_outputDirectory.isEmpty()) {
        // Desactivation of the other processes button
        ui->maskButton->setEnabled(false);
        ui->pointButton->setEnabled(false);
        ui->reconstructionButton->setEnabled(false);

        // Selection of the plyPath
        QString plyPath = _outputDirectory + "/PMVS/models/pmvs_options.txt";
        // Execution of the process
        _filteringProcessed = _filterEngine.runFilter(plyPath.toStdString());

        // Reactivation of the other processes button
        if (_filteringProcessed) {
            ui->pointButton->setEnabled(true);
            ui->maskButton->setEnabled(true);
            ui->filteringButton->setEnabled(true);
            ui->reconstructionButton->setEnabled(true);
        }

    } else {
        std::cout << "Output Directory Empty" << std::endl;
    }
}

void MainWindow::modelReconstruction()
{
    // Check if the oututDirectory is defined and if the other processes have been executed
    if (!_outputDirectory.isEmpty()) {
        // Desactivation of the other processes button
        ui->pointButton->setEnabled(false);
        ui->maskButton->setEnabled(false);
        ui->filteringButton->setEnabled(false);

        // Selection of the directories
        QString inputCam = _outputDirectory + "/PMVS/txt/";
        QString inputMask = _outputDirectory + "/masks/";
        QString plyPath = _outputDirectory + "/PMVS/models/pmvs_options.txtinlier.ply";
        // Execution of the process
        _meshReconstructionEngine.setInPutDirectoryCamera(inputCam.toStdString());
        _meshReconstructionEngine.setInPutDirectoryMask(inputMask.toStdString());
        _meshReconstructionEngine.setInPutFilePly(plyPath.toStdString());
        _meshReconstructionProcessed = _meshReconstructionEngine.Process();
    } else {
        std::cout << "Output Directory Empty" << std::endl;
    }
}




