#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QFileDialog>
#include <QSignalMapper>
#include <QTextStream>
#include <QDir>

//Modules
#include "omvgPmvsReconstructionEngine.h"
#include "filterEngine.hpp"
#include "MaskCreationModule.hpp"
#include "MeshImprimator.h"
#include "src/MeshTools/meshReconstructionEngine.h"


//Callback
#include "src/CallbackTools/pointReconstructionCallback.h"
#include "src/CallbackTools/meshReconstructionCallback.h"
#include <QThread>

//Creation of this class to allow threading
class QomvgReconstructionEngine : public QObject,
                                  public omvgPmvsReconstructionEngine
{
    Q_OBJECT
public:
    QomvgReconstructionEngine(const std::string sInputDirectory,
                              const std::string sOutputDirectory,
                              const std::string sSensorWidthDatabase,
                              const int sFocalLength = -1)
        :omvgPmvsReconstructionEngine(sInputDirectory,
                                      sOutputDirectory,
                                      sSensorWidthDatabase,
                                      sFocalLength)
    {

    }

public slots:
    void process() {this->Process();}
};

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

public slots:
    // Select the sensorDataBase File
    void onSensorDatabasePathClicked();
    // Select the input/output directory
    void openDirectory(QString value);

    // Function calling the maskCreation module constructor and functions
    void maskCreation();
    // Function calling the omvgPmvsReconstructionEngine module constructor and functions
    void pointReconstruction();
    // Function calling the filteringEngine module constructor and functions
    void filtering();
    // Function calling the meshReconstructionEngine module constructor and functions
    void modelReconstruction();
    // Called when Point Cloud reconstruction is finished
    void onReconstructionFinished();
    // Called when Mesh Reconstruction if finished
    void onMeshReconstructionFinished();

    // Settings parameters of the omvgPmvsReconstructionEngine
    void onFocalLengthChanged(int newval);
    void onNearestNeighborDRChanged(double newval);
    void onOctaveMinusOneClicked(bool checked);
    void onPeakTresholdChanged(double newval);
    void onRefinePpDistoClicked(bool checked);
    void onPmvsPathButtonClicked();

    // Callback console viewer
    void setStatus(std::string status);
    void handleError(std::string errorMessage);

private:
    Ui::MainWindow *ui;

    //Point Cloud Reconstruction Engine
    QomvgReconstructionEngine _reconstructionEngine;
    //Mesh Reconstruction Engine
    meshReconstructionEngine _meshReconstructionEngine;
    //Filter Engine
    FilterEngine _filterEngine;
    //MaskCreation Engine
    MaskCreationModule _maskCreationEngine;

    pointReconstructionCallback _pointReconstructionCallback;
    meshReconstructionCallback _meshReconstructionCallback;
    QThread _threadEngine;

    QString _inputDirectory;
    QString _outputDirectory;
    QString _sensorDB;
    QString _stlFile;

    bool _reconstructionProcessed;
    bool _filteringProcessed;
//    bool _maskCreationProcessed;
    bool _meshReconstructionProcessed;

};

#endif // MAINWINDOW_H
