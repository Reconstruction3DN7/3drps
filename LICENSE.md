#3DRPS - 3D Reconstitution from Picture Set 
[`3DRPS`](https://bitbucket.org/Reconstruction3DN7/3drps) is a C++ program and a set of libraries
to graphically reconstruct a mesh from a set of photos.

   * Copyright (C) 2014 :
* Clément Aymard
* Guilhem Marion
* Pierre Fortassin
* Arthur Thollon

This program is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.


***
#OpenMVG

3DRPS makes intensive use of Pierre Moulon's OpenMVG library.

[`OpenMVG`](https://github.com/openMVG/openMVG) is distributed under the Mozilla Public License v2.0 (see [`LICENSE.openMVG`](https://github.com/openMVG/openMVG/blob/master/license.openMVG))

***
#MeshImprimator

To perform point cloud to printable mesh transformation, 3DRPS uses alhanater's MeshImprimator

[`MeshImprimator`](https://bitbucket.org/alhanater/mesh-imprimator-tools/) is a C++ library to reconstruct a mesh from a set of 3D points. 
Copyright (C) 2014  Florence Ho,
                    Paul Bernuau,
                    Victor Ripplinger,
                    Thibaut Thonet,
                    William Lefrançois

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.

This program is a "Larger Work" combining the TetGen library and the Eigen
library. Each of these libraries' license are included below. 

See the following links for more information about "combination of Covered
Software with a work governed by one or more Secondary Licenses":

  * [http://www.mozilla.org/MPL/2.0/FAQ.html#mpl-and-lgpl](http://www.mozilla.org/MPL/2.0/FAQ.html#mpl-and-lgpl)
  * [http://www.gnu.org/licenses/license-list.en.html#MPL-2.0](http://www.gnu.org/licenses/license-list.en.html#MPL-2.0)
  
Some of the provided examples (under the Tools directory) also include the
Freeglut library.

***
#CMVS-PMVS

Dense reconstruction is performed using PMVS2, bundled with CMVS in Pierre Moulon's modified version of Yasutaka Furukawa' CMVS.

[`CMVS-PMVS`](https://github.com/TheFrenchLeaf/CMVS-PMVS) is a modified version of [CMVS](http://www.di.ens.fr/cmvs/) that builds on Windows. CMVS and PMVS2 are distributed under the [GNU General Public License](http://www.gnu.org/copyleft/gpl.html).

***
#PointCloudLibrary

[`PointCloudLibrary`](http://pointclouds.org/) is available freely under the terms of the BSD license. For more information, see [PCL's license file](https://github.com/PointCloudLibrary/pcl/blob/master/LICENSE.txt).

Point Cloud Library (PCL) - [www.pointclouds.org](www.pointclouds.org)

* Copyright (c) 2009-2012, Willow Garage, Inc.
* Copyright (c) 2012-, Open Perception, Inc.
* Copyright (c) XXX, respective authors.