# 3D Reconstruction from Picture Set

3DRPS (3D Reconstruction from Picture Set) is a program aimed at reconstructing a 3D environment or object from a set of pictures, and at preparing it for impression on a 3D printer thanks to Pierre Moulon's `OpenMVG` (structure-from-motion), Yasutaka Furukawa's `PMVS` (dense reconstruction) and alhanater's `MeshImprimatorTools`.

# Quick Start

##Downloading

Don't forget to initiate every submodule and submodule of submodule (eg. this requires `OpenMVG`, which in turn requires `OpenExif`). 
Using 

		$ git clone --recursive https://bitbucket.org/Reconstruction3DN7/3drps.git
		
should do the trick but if `OpenExif` and `glfw` are causing problems you might be required to use 

		$ git submodule update --init --recursive

in the repository's root dir.

## Building

See [`BUILD.md`](https://bitbucket.org/Reconstruction3DN7/3drps/src/master/BUILD.md)

# License

See [`LICENSE.md`](https://bitbucket.org/Reconstruction3DN7/3drps/src/master/LICENSE.md)

# Acknowledgements

We would like to thank Pierre Moulon for his awesome work (and help :)) with OpenMVG and CMVS-PMVS, and Yasutaka Furukawa for his original work on CMVS and PMVS.

# Contact

TBD

# Citations

Moulon Pierre, Monasse Pascal and Marlet Renaud. ACCV 2012.
[Adaptive Structure from Motion with a contrario model estimation.](http://hal.archives-ouvertes.fr/index.php?halsid=1n2qdqiv2a0l5eq7qpos9us752&view_this_doc=hal-00769266&version=1)