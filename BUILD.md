# 3DRPS (3D Reconstruction from Picture Set)

# Build instructions

Requirements :

* `Git`
* `Cmake`
* `OpenCV`
* `Qt5` in your PATH (see [this tutorial](http://www.wikihow.com/Install-Qt-SDK-on-Ubuntu-Linux) if you are having problems)
* The complete set of required libraries for `MeshImprimatorTools` to build.
* The complete set of required libraries for `OpenMVG` to build. 

You can batch-install all these libs on Ubuntu with

	$ sudo apt-get install libpng-dev libjpeg-dev libxxf86vm1 libxxf86vm-dev libxi-dev libxrandr-dev libtet1.4-dev
	
You will also need [`PointCloudLibrary`](http://pointclouds.org/downloads/) (for filtering), which can be installed on Ubuntu with :

	$ sudo add-apt-repository ppa:v-launchpad-jochen-sprickerhof-de/pcl
	$ sudo apt-get update
	$ sudo apt-get install libpcl-all

## Getting the sources

`3DRPS` uses a recursive git dependency structure. Thus you need to initialize and update each of them.
Either do :

	$ git clone https://bitbucket.org/Reconstruction3DN7/3drps.git --recursive

Or

	$ git clone https://bitbucket.org/Reconstruction3DN7/3drps.git
	$ git submodule update --init --recursive

## Building on Linux

Simply cmake/make `3DRPS` :

	$ mkdir build && cd build
	$ cmake ..
	$ make -j <your number of CPUs>
	
If you don't disable building the GUI with `cmake` (option BUILD_GUI), you can simplify the compilation process by replacing the last `make` line with :

	$ mkdir build && cd build
	$ cmake ..
	$ make gui -j <your number of CPUs>

## Building on Windows

Does not work yet.

## Building on Mac

Not yet tested.